﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

using System.Diagnostics;

namespace Updater
{
    class Program
    {
        static void Main(string[] args) {
            String ProgramLocation = args[0];
            Console.WriteLine(ProgramLocation);
            Console.WriteLine("Deleting Previous Version...");
            File.Delete(ProgramLocation);
            Console.WriteLine("Downloading New Version...");
            using (var client = new WebClient()) {
                client.DownloadFile("http://www.jamesonrgrieve.me/versioning/socialstudies/SocialStudiesSoftware.exe", ProgramLocation);
            }
            Console.Write("Press enter to exit the updater and launch the new version...");  Console.Read();
            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments = "";
            //start.Arguments = '"'+Application.ExecutablePath+'"';
            // Enter the executable to run, including the complete path
            start.FileName = ProgramLocation;
            // Do you want to show a console window?
            start.WindowStyle = ProcessWindowStyle.Normal;
            start.CreateNoWindow = false;
            // Run the external process & wait for it to finish
            using (Process proc = Process.Start(start)) ;
            
        }
    }
}

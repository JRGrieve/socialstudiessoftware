﻿namespace SocialStudiesSoftware {
    partial class Form_Text {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TextBox_Text = new System.Windows.Forms.TextBox();
            this.Button_Submit = new System.Windows.Forms.Button();
            this.Label_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBox_Text
            // 
            this.TextBox_Text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Text.Location = new System.Drawing.Point(12, 40);
            this.TextBox_Text.Name = "TextBox_Text";
            this.TextBox_Text.Size = new System.Drawing.Size(460, 20);
            this.TextBox_Text.TabIndex = 0;
            // 
            // Button_Submit
            // 
            this.Button_Submit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Submit.Location = new System.Drawing.Point(200, 80);
            this.Button_Submit.Name = "Button_Submit";
            this.Button_Submit.Size = new System.Drawing.Size(100, 25);
            this.Button_Submit.TabIndex = 1;
            this.Button_Submit.Text = "Submit";
            this.Button_Submit.UseVisualStyleBackColor = true;
            this.Button_Submit.Click += new System.EventHandler(this.Button_Submit_Click);
            // 
            // Label_Label
            // 
            this.Label_Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Label.Location = new System.Drawing.Point(200, 6);
            this.Label_Label.MaximumSize = new System.Drawing.Size(100, 25);
            this.Label_Label.MinimumSize = new System.Drawing.Size(100, 25);
            this.Label_Label.Name = "Label_Label";
            this.Label_Label.Size = new System.Drawing.Size(100, 25);
            this.Label_Label.TabIndex = 2;
            this.Label_Label.Text = "Text";
            this.Label_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form_Text
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 112);
            this.Controls.Add(this.Label_Label);
            this.Controls.Add(this.Button_Submit);
            this.Controls.Add(this.TextBox_Text);
            this.Name = "Form_Text";
            this.Text = "Text Entry";
            this.Load += new System.EventHandler(this.TextForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox_Text;
        private System.Windows.Forms.Button Button_Submit;
        private System.Windows.Forms.Label Label_Label;
    }
}
﻿namespace SocialStudiesSoftware {
    partial class Form_Study {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Label_PoliticalRight = new System.Windows.Forms.Label();
            this.Label_PoliticalLeft = new System.Windows.Forms.Label();
            this.Label_EconomicLeft = new System.Windows.Forms.Label();
            this.Label_EconomicRight = new System.Windows.Forms.Label();
            this.Label_SocialLeft = new System.Windows.Forms.Label();
            this.Label_SocialRight = new System.Windows.Forms.Label();
            this.TrackBar_Political = new System.Windows.Forms.TrackBar();
            this.TrackBar_Economic = new System.Windows.Forms.TrackBar();
            this.Button_No = new System.Windows.Forms.Button();
            this.Label_Name = new System.Windows.Forms.Label();
            this.Button_Done = new System.Windows.Forms.Button();
            this.Button_Yes = new System.Windows.Forms.Button();
            this.Button_Submit = new System.Windows.Forms.Button();
            this.TrackBar_Social = new System.Windows.Forms.TrackBar();
            this.Label_Social = new System.Windows.Forms.Label();
            this.Label_Economic = new System.Windows.Forms.Label();
            this.Label_Political = new System.Windows.Forms.Label();
            this.TextBox_Results_Social = new System.Windows.Forms.TextBox();
            this.TextBox_Results_Political = new System.Windows.Forms.TextBox();
            this.TextBox_Results_Economic = new System.Windows.Forms.TextBox();
            this.Label_Results_Social = new System.Windows.Forms.Label();
            this.Label_Results_Economic = new System.Windows.Forms.Label();
            this.Label_Results_Political = new System.Windows.Forms.Label();
            this.GroupBox_Results = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Political)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Economic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Social)).BeginInit();
            this.GroupBox_Results.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label_PoliticalRight
            // 
            this.Label_PoliticalRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_PoliticalRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.Label_PoliticalRight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Label_PoliticalRight.Location = new System.Drawing.Point(900, 115);
            this.Label_PoliticalRight.Name = "Label_PoliticalRight";
            this.Label_PoliticalRight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_PoliticalRight.Size = new System.Drawing.Size(100, 45);
            this.Label_PoliticalRight.TabIndex = 35;
            this.Label_PoliticalRight.Text = "Dictatorship";
            // 
            // Label_PoliticalLeft
            // 
            this.Label_PoliticalLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.Label_PoliticalLeft.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Label_PoliticalLeft.Location = new System.Drawing.Point(-6, 115);
            this.Label_PoliticalLeft.Name = "Label_PoliticalLeft";
            this.Label_PoliticalLeft.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_PoliticalLeft.Size = new System.Drawing.Size(100, 45);
            this.Label_PoliticalLeft.TabIndex = 36;
            this.Label_PoliticalLeft.Text = "Democracy";
            this.Label_PoliticalLeft.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label_EconomicLeft
            // 
            this.Label_EconomicLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.Label_EconomicLeft.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Label_EconomicLeft.Location = new System.Drawing.Point(-6, 195);
            this.Label_EconomicLeft.Name = "Label_EconomicLeft";
            this.Label_EconomicLeft.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_EconomicLeft.Size = new System.Drawing.Size(100, 45);
            this.Label_EconomicLeft.TabIndex = 39;
            this.Label_EconomicLeft.Text = "Centrally\r\nPlanned";
            this.Label_EconomicLeft.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label_EconomicRight
            // 
            this.Label_EconomicRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_EconomicRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.Label_EconomicRight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Label_EconomicRight.Location = new System.Drawing.Point(900, 195);
            this.Label_EconomicRight.Name = "Label_EconomicRight";
            this.Label_EconomicRight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_EconomicRight.Size = new System.Drawing.Size(100, 45);
            this.Label_EconomicRight.TabIndex = 40;
            this.Label_EconomicRight.Text = "Free \r\nEnterprise";
            // 
            // Label_SocialLeft
            // 
            this.Label_SocialLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.Label_SocialLeft.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Label_SocialLeft.Location = new System.Drawing.Point(-6, 275);
            this.Label_SocialLeft.Name = "Label_SocialLeft";
            this.Label_SocialLeft.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_SocialLeft.Size = new System.Drawing.Size(100, 45);
            this.Label_SocialLeft.TabIndex = 37;
            this.Label_SocialLeft.Text = "Collectivist";
            this.Label_SocialLeft.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label_SocialRight
            // 
            this.Label_SocialRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_SocialRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.Label_SocialRight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Label_SocialRight.Location = new System.Drawing.Point(900, 275);
            this.Label_SocialRight.Name = "Label_SocialRight";
            this.Label_SocialRight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_SocialRight.Size = new System.Drawing.Size(100, 45);
            this.Label_SocialRight.TabIndex = 38;
            this.Label_SocialRight.Text = "Individualist";
            // 
            // TrackBar_Political
            // 
            this.TrackBar_Political.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TrackBar_Political.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TrackBar_Political.LargeChange = 1;
            this.TrackBar_Political.Location = new System.Drawing.Point(100, 116);
            this.TrackBar_Political.Maximum = 5;
            this.TrackBar_Political.MaximumSize = new System.Drawing.Size(1000, 0);
            this.TrackBar_Political.Minimum = -5;
            this.TrackBar_Political.Name = "TrackBar_Political";
            this.TrackBar_Political.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TrackBar_Political.Size = new System.Drawing.Size(791, 45);
            this.TrackBar_Political.TabIndex = 30;
            // 
            // TrackBar_Economic
            // 
            this.TrackBar_Economic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TrackBar_Economic.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TrackBar_Economic.LargeChange = 1;
            this.TrackBar_Economic.Location = new System.Drawing.Point(100, 196);
            this.TrackBar_Economic.Maximum = 5;
            this.TrackBar_Economic.MaximumSize = new System.Drawing.Size(1000, 0);
            this.TrackBar_Economic.Minimum = -5;
            this.TrackBar_Economic.Name = "TrackBar_Economic";
            this.TrackBar_Economic.Size = new System.Drawing.Size(791, 45);
            this.TrackBar_Economic.TabIndex = 32;
            // 
            // Button_No
            // 
            this.Button_No.Enabled = false;
            this.Button_No.Location = new System.Drawing.Point(627, 352);
            this.Button_No.Name = "Button_No";
            this.Button_No.Size = new System.Drawing.Size(115, 23);
            this.Button_No.TabIndex = 41;
            this.Button_No.Text = "Not Yet.";
            this.Button_No.UseVisualStyleBackColor = true;
            this.Button_No.Click += new System.EventHandler(this.Button_No_Click);
            // 
            // Label_Name
            // 
            this.Label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Name.Location = new System.Drawing.Point(3, 3);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(984, 73);
            this.Label_Name.TabIndex = 42;
            this.Label_Name.Text = "name";
            this.Label_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Button_Done
            // 
            this.Button_Done.Location = new System.Drawing.Point(923, 356);
            this.Button_Done.Name = "Button_Done";
            this.Button_Done.Size = new System.Drawing.Size(75, 23);
            this.Button_Done.TabIndex = 43;
            this.Button_Done.Text = "Done";
            this.Button_Done.UseVisualStyleBackColor = true;
            this.Button_Done.Click += new System.EventHandler(this.Button_Done_Click);
            // 
            // Button_Yes
            // 
            this.Button_Yes.Location = new System.Drawing.Point(505, 352);
            this.Button_Yes.Name = "Button_Yes";
            this.Button_Yes.Size = new System.Drawing.Size(115, 23);
            this.Button_Yes.TabIndex = 44;
            this.Button_Yes.Text = "I Know This!";
            this.Button_Yes.UseVisualStyleBackColor = true;
            this.Button_Yes.Click += new System.EventHandler(this.Button_Yes_Click);
            // 
            // Button_Submit
            // 
            this.Button_Submit.Location = new System.Drawing.Point(505, 323);
            this.Button_Submit.Name = "Button_Submit";
            this.Button_Submit.Size = new System.Drawing.Size(237, 23);
            this.Button_Submit.TabIndex = 45;
            this.Button_Submit.Text = "Submit";
            this.Button_Submit.UseVisualStyleBackColor = true;
            this.Button_Submit.Click += new System.EventHandler(this.Button_Submit_Click);
            // 
            // TrackBar_Social
            // 
            this.TrackBar_Social.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TrackBar_Social.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TrackBar_Social.LargeChange = 1;
            this.TrackBar_Social.Location = new System.Drawing.Point(100, 276);
            this.TrackBar_Social.Maximum = 5;
            this.TrackBar_Social.MaximumSize = new System.Drawing.Size(1000, 0);
            this.TrackBar_Social.Minimum = -5;
            this.TrackBar_Social.Name = "TrackBar_Social";
            this.TrackBar_Social.Size = new System.Drawing.Size(791, 45);
            this.TrackBar_Social.TabIndex = 46;
            // 
            // Label_Social
            // 
            this.Label_Social.AutoSize = true;
            this.Label_Social.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Social.Location = new System.Drawing.Point(470, 254);
            this.Label_Social.Name = "Label_Social";
            this.Label_Social.Size = new System.Drawing.Size(55, 18);
            this.Label_Social.TabIndex = 47;
            this.Label_Social.Text = "Social";
            this.Label_Social.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label_Economic
            // 
            this.Label_Economic.AutoSize = true;
            this.Label_Economic.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Economic.Location = new System.Drawing.Point(455, 174);
            this.Label_Economic.Name = "Label_Economic";
            this.Label_Economic.Size = new System.Drawing.Size(84, 18);
            this.Label_Economic.TabIndex = 48;
            this.Label_Economic.Text = "Economic";
            this.Label_Economic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label_Political
            // 
            this.Label_Political.AutoSize = true;
            this.Label_Political.Cursor = System.Windows.Forms.Cursors.No;
            this.Label_Political.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Political.Location = new System.Drawing.Point(463, 94);
            this.Label_Political.Name = "Label_Political";
            this.Label_Political.Size = new System.Drawing.Size(68, 18);
            this.Label_Political.TabIndex = 49;
            this.Label_Political.Text = "Political";
            this.Label_Political.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TextBox_Results_Social
            // 
            this.TextBox_Results_Social.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.TextBox_Results_Social.Location = new System.Drawing.Point(303, 49);
            this.TextBox_Results_Social.Name = "TextBox_Results_Social";
            this.TextBox_Results_Social.ReadOnly = true;
            this.TextBox_Results_Social.Size = new System.Drawing.Size(140, 20);
            this.TextBox_Results_Social.TabIndex = 50;
            this.TextBox_Results_Social.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TextBox_Results_Political
            // 
            this.TextBox_Results_Political.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.TextBox_Results_Political.Location = new System.Drawing.Point(11, 49);
            this.TextBox_Results_Political.Name = "TextBox_Results_Political";
            this.TextBox_Results_Political.ReadOnly = true;
            this.TextBox_Results_Political.Size = new System.Drawing.Size(140, 20);
            this.TextBox_Results_Political.TabIndex = 51;
            this.TextBox_Results_Political.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TextBox_Results_Economic
            // 
            this.TextBox_Results_Economic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.TextBox_Results_Economic.Location = new System.Drawing.Point(157, 49);
            this.TextBox_Results_Economic.Name = "TextBox_Results_Economic";
            this.TextBox_Results_Economic.ReadOnly = true;
            this.TextBox_Results_Economic.Size = new System.Drawing.Size(140, 20);
            this.TextBox_Results_Economic.TabIndex = 52;
            this.TextBox_Results_Economic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label_Results_Social
            // 
            this.Label_Results_Social.AutoSize = true;
            this.Label_Results_Social.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Results_Social.Location = new System.Drawing.Point(351, 30);
            this.Label_Results_Social.Name = "Label_Results_Social";
            this.Label_Results_Social.Size = new System.Drawing.Size(46, 16);
            this.Label_Results_Social.TabIndex = 53;
            this.Label_Results_Social.Text = "Social";
            this.Label_Results_Social.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label_Results_Economic
            // 
            this.Label_Results_Economic.AutoSize = true;
            this.Label_Results_Economic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Results_Economic.Location = new System.Drawing.Point(192, 28);
            this.Label_Results_Economic.Name = "Label_Results_Economic";
            this.Label_Results_Economic.Size = new System.Drawing.Size(68, 16);
            this.Label_Results_Economic.TabIndex = 54;
            this.Label_Results_Economic.Text = "Economic";
            this.Label_Results_Economic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label_Results_Political
            // 
            this.Label_Results_Political.AutoSize = true;
            this.Label_Results_Political.Cursor = System.Windows.Forms.Cursors.No;
            this.Label_Results_Political.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Results_Political.Location = new System.Drawing.Point(50, 28);
            this.Label_Results_Political.Name = "Label_Results_Political";
            this.Label_Results_Political.Size = new System.Drawing.Size(55, 16);
            this.Label_Results_Political.TabIndex = 55;
            this.Label_Results_Political.Text = "Political";
            this.Label_Results_Political.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GroupBox_Results
            // 
            this.GroupBox_Results.Controls.Add(this.Label_Results_Economic);
            this.GroupBox_Results.Controls.Add(this.TextBox_Results_Social);
            this.GroupBox_Results.Controls.Add(this.Label_Results_Political);
            this.GroupBox_Results.Controls.Add(this.TextBox_Results_Political);
            this.GroupBox_Results.Controls.Add(this.TextBox_Results_Economic);
            this.GroupBox_Results.Controls.Add(this.Label_Results_Social);
            this.GroupBox_Results.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox_Results.Location = new System.Drawing.Point(16, 302);
            this.GroupBox_Results.Name = "GroupBox_Results";
            this.GroupBox_Results.Size = new System.Drawing.Size(452, 87);
            this.GroupBox_Results.TabIndex = 57;
            this.GroupBox_Results.TabStop = false;
            this.GroupBox_Results.Text = "Results";
            // 
            // Form_Study
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 392);
            this.Controls.Add(this.GroupBox_Results);
            this.Controls.Add(this.Label_Political);
            this.Controls.Add(this.Label_Economic);
            this.Controls.Add(this.Label_Social);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.TrackBar_Social);
            this.Controls.Add(this.Button_Submit);
            this.Controls.Add(this.Button_Yes);
            this.Controls.Add(this.Button_Done);
            this.Controls.Add(this.Button_No);
            this.Controls.Add(this.Label_PoliticalRight);
            this.Controls.Add(this.Label_PoliticalLeft);
            this.Controls.Add(this.Label_EconomicLeft);
            this.Controls.Add(this.Label_EconomicRight);
            this.Controls.Add(this.Label_SocialLeft);
            this.Controls.Add(this.Label_SocialRight);
            this.Controls.Add(this.TrackBar_Political);
            this.Controls.Add(this.TrackBar_Economic);
            this.MaximumSize = new System.Drawing.Size(1024, 430);
            this.MinimumSize = new System.Drawing.Size(1024, 430);
            this.Name = "Form_Study";
            this.Text = "Form_Study";
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Political)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Economic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Social)).EndInit();
            this.GroupBox_Results.ResumeLayout(false);
            this.GroupBox_Results.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_PoliticalRight;
        private System.Windows.Forms.Label Label_PoliticalLeft;
        private System.Windows.Forms.Label Label_EconomicLeft;
        private System.Windows.Forms.Label Label_EconomicRight;
        private System.Windows.Forms.Label Label_SocialLeft;
        private System.Windows.Forms.Label Label_SocialRight;
        private System.Windows.Forms.TrackBar TrackBar_Political;
        private System.Windows.Forms.TrackBar TrackBar_Economic;
        private System.Windows.Forms.Button Button_No;
        private System.Windows.Forms.Label Label_Name;
        private System.Windows.Forms.Button Button_Done;
        private System.Windows.Forms.Button Button_Yes;
        private System.Windows.Forms.Button Button_Submit;
        private System.Windows.Forms.TrackBar TrackBar_Social;
        private System.Windows.Forms.Label Label_Social;
        private System.Windows.Forms.Label Label_Economic;
        private System.Windows.Forms.Label Label_Political;
        private System.Windows.Forms.TextBox TextBox_Results_Social;
        private System.Windows.Forms.TextBox TextBox_Results_Political;
        private System.Windows.Forms.TextBox TextBox_Results_Economic;
        private System.Windows.Forms.Label Label_Results_Social;
        private System.Windows.Forms.Label Label_Results_Economic;
        private System.Windows.Forms.Label Label_Results_Political;
        private System.Windows.Forms.GroupBox GroupBox_Results;
    }
}
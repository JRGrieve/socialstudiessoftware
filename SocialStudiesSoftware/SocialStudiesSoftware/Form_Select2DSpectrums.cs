﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocialStudiesSoftware {
    public partial class Form_Select2DSpectrums : Form {
        public int EnabledNum = 3;
        public Form_Select2DSpectrums() {
            InitializeComponent();
        }

        private void Button_Submit_Click(object sender, EventArgs e) {
            if (CheckBox_Political.Checked && CheckBox_Economic.Checked) {
                HiddenString.Text = "1,2";
            } else if (CheckBox_Political.Checked && CheckBox_Social.Checked)  {
                HiddenString.Text = "1,3";
            } else if (CheckBox_Economic.Checked && CheckBox_Social.Checked) {
                HiddenString.Text = "2,3";
            } else {
                HiddenString.Text = "nil";
            }
            this.Close();
        }

        private void Checkbox_Social_CheckedChanged(object sender, EventArgs e) {
            if (CheckBox_Social.Checked) {
                EnabledNum += 1;
            } else {
                EnabledNum -= 1;
            }
            if (EnabledNum == 2) {
                Button_Submit.Enabled = true;
            } else {
                Button_Submit.Enabled = false;
            }
        }

        private void Checkbox_Economic_CheckedChanged(object sender, EventArgs e) {
            if (CheckBox_Economic.Checked) {
                EnabledNum += 1;
            } else {
                EnabledNum -= 1;
            }
            if (EnabledNum == 2) {
                Button_Submit.Enabled = true;
            } else {
                Button_Submit.Enabled = false;
            }
        }

        private void Checkbox_Political_CheckedChanged(object sender, EventArgs e) {
            if (CheckBox_Political.Checked) {
                EnabledNum += 1;
            } else {
                EnabledNum -= 1;
            }
            if (EnabledNum == 2) {
                Button_Submit.Enabled = true;
            } else {
                Button_Submit.Enabled = false;
            }
        }
    }
}

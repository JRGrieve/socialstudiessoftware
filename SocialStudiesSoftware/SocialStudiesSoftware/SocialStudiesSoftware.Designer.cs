﻿namespace SocialStudiesSoftware
{
    partial class SocialStudiesSoftware
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SocialStudiesSoftware));
            this.Label_Social = new System.Windows.Forms.Label();
            this.Label_SavedPersons = new System.Windows.Forms.Label();
            this.Button_CreateNew = new System.Windows.Forms.Button();
            this.ListBox_SocialPeople = new System.Windows.Forms.ListBox();
            this.TrackBar_Political = new System.Windows.Forms.TrackBar();
            this.TrackBar_Social = new System.Windows.Forms.TrackBar();
            this.TrackBar_Economic = new System.Windows.Forms.TrackBar();
            this.TextBox_Name = new System.Windows.Forms.TextBox();
            this.Label_Economic = new System.Windows.Forms.Label();
            this.Label_Political = new System.Windows.Forms.Label();
            this.Label_PoliticalRight = new System.Windows.Forms.Label();
            this.Label_PoliticalLeft = new System.Windows.Forms.Label();
            this.Label_SocialLeft = new System.Windows.Forms.Label();
            this.Label_SocialRight = new System.Windows.Forms.Label();
            this.Label_EconomicLeft = new System.Windows.Forms.Label();
            this.Label_EconomicRight = new System.Windows.Forms.Label();
            this.TreeView_Notes = new System.Windows.Forms.TreeView();
            this.GroupBox_Data = new System.Windows.Forms.GroupBox();
            this.Label_TextSize = new System.Windows.Forms.Label();
            this.TrackBar_TextSize = new System.Windows.Forms.TrackBar();
            this.Button_RenderSelected2D = new System.Windows.Forms.Button();
            this.Button_Down = new System.Windows.Forms.Button();
            this.Button_Up = new System.Windows.Forms.Button();
            this.Button_RenderSelected1D = new System.Windows.Forms.Button();
            this.Button_RemoveSelected = new System.Windows.Forms.Button();
            this.Button_OverwriteSelected = new System.Windows.Forms.Button();
            this.Button_LoadSelected = new System.Windows.Forms.Button();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.MenuBar = new System.Windows.Forms.MenuStrip();
            this.ToolStrip_File = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_File_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_File_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_File_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_Info = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_Misc_CheckForUpdates = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_Misc_Credits = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_Study = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_Study_Begin = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlOne = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TreeView_Notes_CM_LvlOne_Create = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlOne_Parse = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlOne_Clear = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TreeView_Notes_CM_LvlTwo_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Up = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Top = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Down = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Bottom = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Category = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Biographical = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Political = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Economic = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Social = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckBox_Political = new System.Windows.Forms.CheckBox();
            this.CheckBox_Economic = new System.Windows.Forms.CheckBox();
            this.CheckBox_Social = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RadioButton_Ideology = new System.Windows.Forms.RadioButton();
            this.RadioButton_Philosopher = new System.Windows.Forms.RadioButton();
            this.ToolStrip_Study_AllowClose = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Political)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Social)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Economic)).BeginInit();
            this.GroupBox_Data.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_TextSize)).BeginInit();
            this.MenuBar.SuspendLayout();
            this.TreeView_Notes_CM_LvlOne.SuspendLayout();
            this.TreeView_Notes_CM_LvlTwo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label_Social
            // 
            resources.ApplyResources(this.Label_Social, "Label_Social");
            this.Label_Social.Name = "Label_Social";
            // 
            // Label_SavedPersons
            // 
            resources.ApplyResources(this.Label_SavedPersons, "Label_SavedPersons");
            this.Label_SavedPersons.Name = "Label_SavedPersons";
            // 
            // Button_CreateNew
            // 
            resources.ApplyResources(this.Button_CreateNew, "Button_CreateNew");
            this.Button_CreateNew.Name = "Button_CreateNew";
            this.Button_CreateNew.UseVisualStyleBackColor = true;
            this.Button_CreateNew.Click += new System.EventHandler(this.Button_CreateNew_Click);
            // 
            // ListBox_SocialPeople
            // 
            resources.ApplyResources(this.ListBox_SocialPeople, "ListBox_SocialPeople");
            this.ListBox_SocialPeople.FormattingEnabled = true;
            this.ListBox_SocialPeople.Name = "ListBox_SocialPeople";
            // 
            // TrackBar_Political
            // 
            resources.ApplyResources(this.TrackBar_Political, "TrackBar_Political");
            this.TrackBar_Political.LargeChange = 1;
            this.TrackBar_Political.Maximum = 5;
            this.TrackBar_Political.Minimum = -5;
            this.TrackBar_Political.Name = "TrackBar_Political";
            // 
            // TrackBar_Social
            // 
            resources.ApplyResources(this.TrackBar_Social, "TrackBar_Social");
            this.TrackBar_Social.LargeChange = 1;
            this.TrackBar_Social.Maximum = 5;
            this.TrackBar_Social.Minimum = -5;
            this.TrackBar_Social.Name = "TrackBar_Social";
            // 
            // TrackBar_Economic
            // 
            resources.ApplyResources(this.TrackBar_Economic, "TrackBar_Economic");
            this.TrackBar_Economic.LargeChange = 1;
            this.TrackBar_Economic.Maximum = 5;
            this.TrackBar_Economic.Minimum = -5;
            this.TrackBar_Economic.Name = "TrackBar_Economic";
            // 
            // TextBox_Name
            // 
            resources.ApplyResources(this.TextBox_Name, "TextBox_Name");
            this.TextBox_Name.Name = "TextBox_Name";
            // 
            // Label_Economic
            // 
            resources.ApplyResources(this.Label_Economic, "Label_Economic");
            this.Label_Economic.Name = "Label_Economic";
            // 
            // Label_Political
            // 
            resources.ApplyResources(this.Label_Political, "Label_Political");
            this.Label_Political.Name = "Label_Political";
            // 
            // Label_PoliticalRight
            // 
            resources.ApplyResources(this.Label_PoliticalRight, "Label_PoliticalRight");
            this.Label_PoliticalRight.Name = "Label_PoliticalRight";
            // 
            // Label_PoliticalLeft
            // 
            resources.ApplyResources(this.Label_PoliticalLeft, "Label_PoliticalLeft");
            this.Label_PoliticalLeft.Name = "Label_PoliticalLeft";
            // 
            // Label_SocialLeft
            // 
            resources.ApplyResources(this.Label_SocialLeft, "Label_SocialLeft");
            this.Label_SocialLeft.Name = "Label_SocialLeft";
            // 
            // Label_SocialRight
            // 
            resources.ApplyResources(this.Label_SocialRight, "Label_SocialRight");
            this.Label_SocialRight.Name = "Label_SocialRight";
            // 
            // Label_EconomicLeft
            // 
            resources.ApplyResources(this.Label_EconomicLeft, "Label_EconomicLeft");
            this.Label_EconomicLeft.Name = "Label_EconomicLeft";
            // 
            // Label_EconomicRight
            // 
            resources.ApplyResources(this.Label_EconomicRight, "Label_EconomicRight");
            this.Label_EconomicRight.Name = "Label_EconomicRight";
            // 
            // TreeView_Notes
            // 
            resources.ApplyResources(this.TreeView_Notes, "TreeView_Notes");
            this.TreeView_Notes.Name = "TreeView_Notes";
            this.TreeView_Notes.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("TreeView_Notes.Nodes")))});
            this.TreeView_Notes.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.TreeView_Notes_AfterLabelEdit);
            this.TreeView_Notes.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView_Notes_NodeMouseClick);
            this.TreeView_Notes.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TreeView_Notes_KeyUp);
            // 
            // GroupBox_Data
            // 
            resources.ApplyResources(this.GroupBox_Data, "GroupBox_Data");
            this.GroupBox_Data.Controls.Add(this.Label_TextSize);
            this.GroupBox_Data.Controls.Add(this.TrackBar_TextSize);
            this.GroupBox_Data.Controls.Add(this.Button_RenderSelected2D);
            this.GroupBox_Data.Controls.Add(this.Button_Down);
            this.GroupBox_Data.Controls.Add(this.Button_Up);
            this.GroupBox_Data.Controls.Add(this.Button_RenderSelected1D);
            this.GroupBox_Data.Controls.Add(this.Button_RemoveSelected);
            this.GroupBox_Data.Controls.Add(this.Button_OverwriteSelected);
            this.GroupBox_Data.Controls.Add(this.Button_LoadSelected);
            this.GroupBox_Data.Controls.Add(this.ListBox_SocialPeople);
            this.GroupBox_Data.Controls.Add(this.Button_CreateNew);
            this.GroupBox_Data.Controls.Add(this.Label_SavedPersons);
            this.GroupBox_Data.Name = "GroupBox_Data";
            this.GroupBox_Data.TabStop = false;
            // 
            // Label_TextSize
            // 
            resources.ApplyResources(this.Label_TextSize, "Label_TextSize");
            this.Label_TextSize.Name = "Label_TextSize";
            // 
            // TrackBar_TextSize
            // 
            resources.ApplyResources(this.TrackBar_TextSize, "TrackBar_TextSize");
            this.TrackBar_TextSize.Maximum = 5;
            this.TrackBar_TextSize.Minimum = -5;
            this.TrackBar_TextSize.Name = "TrackBar_TextSize";
            // 
            // Button_RenderSelected2D
            // 
            resources.ApplyResources(this.Button_RenderSelected2D, "Button_RenderSelected2D");
            this.Button_RenderSelected2D.Name = "Button_RenderSelected2D";
            this.Button_RenderSelected2D.UseVisualStyleBackColor = true;
            this.Button_RenderSelected2D.Click += new System.EventHandler(this.Button_RenderSelected2D_Click);
            // 
            // Button_Down
            // 
            resources.ApplyResources(this.Button_Down, "Button_Down");
            this.Button_Down.Name = "Button_Down";
            this.Button_Down.UseVisualStyleBackColor = true;
            this.Button_Down.Click += new System.EventHandler(this.Button_Down_Click);
            // 
            // Button_Up
            // 
            resources.ApplyResources(this.Button_Up, "Button_Up");
            this.Button_Up.Name = "Button_Up";
            this.Button_Up.UseVisualStyleBackColor = true;
            this.Button_Up.Click += new System.EventHandler(this.Button_Up_Click);
            // 
            // Button_RenderSelected1D
            // 
            resources.ApplyResources(this.Button_RenderSelected1D, "Button_RenderSelected1D");
            this.Button_RenderSelected1D.Name = "Button_RenderSelected1D";
            this.Button_RenderSelected1D.UseVisualStyleBackColor = true;
            this.Button_RenderSelected1D.Click += new System.EventHandler(this.Button_RenderSelected1D_Click);
            // 
            // Button_RemoveSelected
            // 
            resources.ApplyResources(this.Button_RemoveSelected, "Button_RemoveSelected");
            this.Button_RemoveSelected.Name = "Button_RemoveSelected";
            this.Button_RemoveSelected.UseVisualStyleBackColor = true;
            this.Button_RemoveSelected.Click += new System.EventHandler(this.Button_RemoveSelected_Click);
            // 
            // Button_OverwriteSelected
            // 
            resources.ApplyResources(this.Button_OverwriteSelected, "Button_OverwriteSelected");
            this.Button_OverwriteSelected.Name = "Button_OverwriteSelected";
            this.Button_OverwriteSelected.UseVisualStyleBackColor = true;
            this.Button_OverwriteSelected.Click += new System.EventHandler(this.Button_OverwriteSelected_Click);
            // 
            // Button_LoadSelected
            // 
            resources.ApplyResources(this.Button_LoadSelected, "Button_LoadSelected");
            this.Button_LoadSelected.Name = "Button_LoadSelected";
            this.Button_LoadSelected.UseVisualStyleBackColor = true;
            this.Button_LoadSelected.Click += new System.EventHandler(this.Button_LoadSelected_Click);
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.DefaultExt = "SSDat";
            resources.ApplyResources(this.OpenFileDialog, "OpenFileDialog");
            // 
            // SaveFileDialog
            // 
            this.SaveFileDialog.DefaultExt = "SSDat";
            resources.ApplyResources(this.SaveFileDialog, "SaveFileDialog");
            this.SaveFileDialog.ValidateNames = false;
            // 
            // MenuBar
            // 
            this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStrip_File,
            this.ToolStrip_Info,
            this.ToolStrip_Study});
            resources.ApplyResources(this.MenuBar, "MenuBar");
            this.MenuBar.Name = "MenuBar";
            // 
            // ToolStrip_File
            // 
            this.ToolStrip_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStrip_File_Save,
            this.ToolStrip_File_SaveAs,
            this.ToolStrip_File_Open});
            this.ToolStrip_File.Name = "ToolStrip_File";
            resources.ApplyResources(this.ToolStrip_File, "ToolStrip_File");
            // 
            // ToolStrip_File_Save
            // 
            this.ToolStrip_File_Save.Name = "ToolStrip_File_Save";
            resources.ApplyResources(this.ToolStrip_File_Save, "ToolStrip_File_Save");
            this.ToolStrip_File_Save.Click += new System.EventHandler(this.MenuBar_File_Save_Click);
            // 
            // ToolStrip_File_SaveAs
            // 
            this.ToolStrip_File_SaveAs.Name = "ToolStrip_File_SaveAs";
            resources.ApplyResources(this.ToolStrip_File_SaveAs, "ToolStrip_File_SaveAs");
            this.ToolStrip_File_SaveAs.Click += new System.EventHandler(this.MenuBar_File_SaveAs_Click);
            // 
            // ToolStrip_File_Open
            // 
            this.ToolStrip_File_Open.Name = "ToolStrip_File_Open";
            resources.ApplyResources(this.ToolStrip_File_Open, "ToolStrip_File_Open");
            this.ToolStrip_File_Open.Click += new System.EventHandler(this.MenuBar_File_Open_Click);
            // 
            // ToolStrip_Info
            // 
            this.ToolStrip_Info.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStrip_Misc_CheckForUpdates,
            this.ToolStrip_Misc_Credits});
            this.ToolStrip_Info.Name = "ToolStrip_Info";
            resources.ApplyResources(this.ToolStrip_Info, "ToolStrip_Info");
            // 
            // ToolStrip_Misc_CheckForUpdates
            // 
            this.ToolStrip_Misc_CheckForUpdates.Name = "ToolStrip_Misc_CheckForUpdates";
            resources.ApplyResources(this.ToolStrip_Misc_CheckForUpdates, "ToolStrip_Misc_CheckForUpdates");
            this.ToolStrip_Misc_CheckForUpdates.Click += new System.EventHandler(this.MenuBar_Misc_Updates_Click);
            // 
            // ToolStrip_Misc_Credits
            // 
            this.ToolStrip_Misc_Credits.Name = "ToolStrip_Misc_Credits";
            resources.ApplyResources(this.ToolStrip_Misc_Credits, "ToolStrip_Misc_Credits");
            this.ToolStrip_Misc_Credits.Click += new System.EventHandler(this.MenuBar_Misc_Credits_Click);
            // 
            // ToolStrip_Study
            // 
            this.ToolStrip_Study.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStrip_Study_Begin,
            this.ToolStrip_Study_AllowClose});
            this.ToolStrip_Study.Name = "ToolStrip_Study";
            resources.ApplyResources(this.ToolStrip_Study, "ToolStrip_Study");
            // 
            // ToolStrip_Study_Begin
            // 
            this.ToolStrip_Study_Begin.Name = "ToolStrip_Study_Begin";
            resources.ApplyResources(this.ToolStrip_Study_Begin, "ToolStrip_Study_Begin");
            this.ToolStrip_Study_Begin.Click += new System.EventHandler(this.testToolStripMenuItem1_Click);
            // 
            // TreeView_Notes_CM_LvlOne
            // 
            this.TreeView_Notes_CM_LvlOne.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TreeView_Notes_CM_LvlOne_Create,
            this.TreeView_Notes_CM_LvlOne_Parse,
            this.TreeView_Notes_CM_LvlOne_Clear});
            this.TreeView_Notes_CM_LvlOne.Name = "TreeView_Notes_ContextMenu";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlOne, "TreeView_Notes_CM_LvlOne");
            this.TreeView_Notes_CM_LvlOne.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.TreeView_Notes_CM_LvlOne_Closed);
            // 
            // TreeView_Notes_CM_LvlOne_Create
            // 
            this.TreeView_Notes_CM_LvlOne_Create.Name = "TreeView_Notes_CM_LvlOne_Create";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlOne_Create, "TreeView_Notes_CM_LvlOne_Create");
            this.TreeView_Notes_CM_LvlOne_Create.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlOne_Create_Click);
            // 
            // TreeView_Notes_CM_LvlOne_Parse
            // 
            this.TreeView_Notes_CM_LvlOne_Parse.Name = "TreeView_Notes_CM_LvlOne_Parse";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlOne_Parse, "TreeView_Notes_CM_LvlOne_Parse");
            this.TreeView_Notes_CM_LvlOne_Parse.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlOne_Parse_Click);
            // 
            // TreeView_Notes_CM_LvlOne_Clear
            // 
            this.TreeView_Notes_CM_LvlOne_Clear.Name = "TreeView_Notes_CM_LvlOne_Clear";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlOne_Clear, "TreeView_Notes_CM_LvlOne_Clear");
            this.TreeView_Notes_CM_LvlOne_Clear.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlOne_Clear_Click);
            // 
            // TreeView_Notes_CM_LvlTwo
            // 
            this.TreeView_Notes_CM_LvlTwo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TreeView_Notes_CM_LvlTwo_Edit,
            this.TreeView_Notes_CM_LvlTwo_Delete,
            this.TreeView_Notes_CM_LvlTwo_Move});
            this.TreeView_Notes_CM_LvlTwo.Name = "TreeView_Notes_CM_LvlTwo";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo, "TreeView_Notes_CM_LvlTwo");
            this.TreeView_Notes_CM_LvlTwo.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.TreeView_Notes_CM_LvlTwo_Closed);
            // 
            // TreeView_Notes_CM_LvlTwo_Edit
            // 
            this.TreeView_Notes_CM_LvlTwo_Edit.Name = "TreeView_Notes_CM_LvlTwo_Edit";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Edit, "TreeView_Notes_CM_LvlTwo_Edit");
            this.TreeView_Notes_CM_LvlTwo_Edit.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Edit_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Delete
            // 
            this.TreeView_Notes_CM_LvlTwo_Delete.Name = "TreeView_Notes_CM_LvlTwo_Delete";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Delete, "TreeView_Notes_CM_LvlTwo_Delete");
            this.TreeView_Notes_CM_LvlTwo_Delete.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Delete_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move
            // 
            this.TreeView_Notes_CM_LvlTwo_Move.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TreeView_Notes_CM_LvlTwo_Move_Up,
            this.TreeView_Notes_CM_LvlTwo_Move_Top,
            this.TreeView_Notes_CM_LvlTwo_Move_Down,
            this.TreeView_Notes_CM_LvlTwo_Move_Bottom,
            this.TreeView_Notes_CM_LvlTwo_Move_Category});
            this.TreeView_Notes_CM_LvlTwo_Move.Name = "TreeView_Notes_CM_LvlTwo_Move";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move, "TreeView_Notes_CM_LvlTwo_Move");
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Up
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Up.Name = "TreeView_Notes_CM_LvlTwo_Move_Up";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Up, "TreeView_Notes_CM_LvlTwo_Move_Up");
            this.TreeView_Notes_CM_LvlTwo_Move_Up.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Up_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Top
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Top.Name = "TreeView_Notes_CM_LvlTwo_Move_Top";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Top, "TreeView_Notes_CM_LvlTwo_Move_Top");
            this.TreeView_Notes_CM_LvlTwo_Move_Top.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Top_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Down
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Down.Name = "TreeView_Notes_CM_LvlTwo_Move_Down";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Down, "TreeView_Notes_CM_LvlTwo_Move_Down");
            this.TreeView_Notes_CM_LvlTwo_Move_Down.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Down_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Bottom
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Bottom.Name = "TreeView_Notes_CM_LvlTwo_Move_Bottom";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Bottom, "TreeView_Notes_CM_LvlTwo_Move_Bottom");
            this.TreeView_Notes_CM_LvlTwo_Move_Bottom.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Bottom_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Category
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Category.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Biographical,
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Political,
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Economic,
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Social});
            this.TreeView_Notes_CM_LvlTwo_Move_Category.Name = "TreeView_Notes_CM_LvlTwo_Move_Category";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Category, "TreeView_Notes_CM_LvlTwo_Move_Category");
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Category_Biographical
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Biographical.Name = "TreeView_Notes_CM_LvlTwo_Move_Category_Biographical";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Category_Biographical, "TreeView_Notes_CM_LvlTwo_Move_Category_Biographical");
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Biographical.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Category_Biographical_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Category_Political
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Political.Name = "TreeView_Notes_CM_LvlTwo_Move_Category_Political";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Category_Political, "TreeView_Notes_CM_LvlTwo_Move_Category_Political");
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Political.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Category_Political_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Category_Economic
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Economic.Name = "TreeView_Notes_CM_LvlTwo_Move_Category_Economic";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Category_Economic, "TreeView_Notes_CM_LvlTwo_Move_Category_Economic");
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Economic.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Category_Economic_Click);
            // 
            // TreeView_Notes_CM_LvlTwo_Move_Category_Social
            // 
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Social.Name = "TreeView_Notes_CM_LvlTwo_Move_Category_Social";
            resources.ApplyResources(this.TreeView_Notes_CM_LvlTwo_Move_Category_Social, "TreeView_Notes_CM_LvlTwo_Move_Category_Social");
            this.TreeView_Notes_CM_LvlTwo_Move_Category_Social.Click += new System.EventHandler(this.TreeView_Notes_CM_LvlTwo_Move_Category_Social_Click);
            // 
            // CheckBox_Political
            // 
            resources.ApplyResources(this.CheckBox_Political, "CheckBox_Political");
            this.CheckBox_Political.Checked = true;
            this.CheckBox_Political.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_Political.Name = "CheckBox_Political";
            this.CheckBox_Political.UseVisualStyleBackColor = true;
            this.CheckBox_Political.CheckedChanged += new System.EventHandler(this.CheckBox_Political_CheckedChanged);
            // 
            // CheckBox_Economic
            // 
            resources.ApplyResources(this.CheckBox_Economic, "CheckBox_Economic");
            this.CheckBox_Economic.Checked = true;
            this.CheckBox_Economic.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_Economic.Name = "CheckBox_Economic";
            this.CheckBox_Economic.UseVisualStyleBackColor = true;
            this.CheckBox_Economic.CheckedChanged += new System.EventHandler(this.CheckBox_Economic_CheckedChanged);
            // 
            // CheckBox_Social
            // 
            resources.ApplyResources(this.CheckBox_Social, "CheckBox_Social");
            this.CheckBox_Social.Checked = true;
            this.CheckBox_Social.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_Social.Name = "CheckBox_Social";
            this.CheckBox_Social.UseVisualStyleBackColor = true;
            this.CheckBox_Social.CheckedChanged += new System.EventHandler(this.CheckBox_Social_CheckedChanged);
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.RadioButton_Ideology);
            this.groupBox1.Controls.Add(this.RadioButton_Philosopher);
            this.groupBox1.Controls.Add(this.CheckBox_Social);
            this.groupBox1.Controls.Add(this.Label_Social);
            this.groupBox1.Controls.Add(this.CheckBox_Economic);
            this.groupBox1.Controls.Add(this.CheckBox_Political);
            this.groupBox1.Controls.Add(this.Label_Economic);
            this.groupBox1.Controls.Add(this.Label_Political);
            this.groupBox1.Controls.Add(this.Label_PoliticalRight);
            this.groupBox1.Controls.Add(this.Label_PoliticalLeft);
            this.groupBox1.Controls.Add(this.Label_EconomicLeft);
            this.groupBox1.Controls.Add(this.Label_EconomicRight);
            this.groupBox1.Controls.Add(this.Label_SocialLeft);
            this.groupBox1.Controls.Add(this.Label_SocialRight);
            this.groupBox1.Controls.Add(this.TextBox_Name);
            this.groupBox1.Controls.Add(this.TrackBar_Social);
            this.groupBox1.Controls.Add(this.TrackBar_Political);
            this.groupBox1.Controls.Add(this.TrackBar_Economic);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // RadioButton_Ideology
            // 
            resources.ApplyResources(this.RadioButton_Ideology, "RadioButton_Ideology");
            this.RadioButton_Ideology.Name = "RadioButton_Ideology";
            this.RadioButton_Ideology.TabStop = true;
            this.RadioButton_Ideology.UseVisualStyleBackColor = true;
            this.RadioButton_Ideology.Click += new System.EventHandler(this.RadioButton_Ideology_Click);
            // 
            // RadioButton_Philosopher
            // 
            resources.ApplyResources(this.RadioButton_Philosopher, "RadioButton_Philosopher");
            this.RadioButton_Philosopher.Name = "RadioButton_Philosopher";
            this.RadioButton_Philosopher.TabStop = true;
            this.RadioButton_Philosopher.UseVisualStyleBackColor = true;
            this.RadioButton_Philosopher.Click += new System.EventHandler(this.RadioButton_Philosopher_Click);
            // 
            // ToolStrip_Study_AllowClose
            // 
            this.ToolStrip_Study_AllowClose.Checked = true;
            this.ToolStrip_Study_AllowClose.CheckOnClick = true;
            this.ToolStrip_Study_AllowClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ToolStrip_Study_AllowClose.Name = "ToolStrip_Study_AllowClose";
            resources.ApplyResources(this.ToolStrip_Study_AllowClose, "ToolStrip_Study_AllowClose");
            this.ToolStrip_Study_AllowClose.Click += new System.EventHandler(this.ToolStrip_Study_AllowClose_Click);
            // 
            // SocialStudiesSoftware
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.GroupBox_Data);
            this.Controls.Add(this.TreeView_Notes);
            this.Controls.Add(this.MenuBar);
            this.Controls.Add(this.groupBox1);
            this.MainMenuStrip = this.MenuBar;
            this.Name = "SocialStudiesSoftware";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SocialStudiesSoftware_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Political)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Social)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Economic)).EndInit();
            this.GroupBox_Data.ResumeLayout(false);
            this.GroupBox_Data.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_TextSize)).EndInit();
            this.MenuBar.ResumeLayout(false);
            this.MenuBar.PerformLayout();
            this.TreeView_Notes_CM_LvlOne.ResumeLayout(false);
            this.TreeView_Notes_CM_LvlTwo.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_Social;
        private System.Windows.Forms.Label Label_SavedPersons;
        private System.Windows.Forms.Button Button_CreateNew;
        private System.Windows.Forms.ListBox ListBox_SocialPeople;
        private System.Windows.Forms.TrackBar TrackBar_Political;
        private System.Windows.Forms.TrackBar TrackBar_Social;
        private System.Windows.Forms.TrackBar TrackBar_Economic;
        private System.Windows.Forms.TextBox TextBox_Name;
        private System.Windows.Forms.Label Label_Economic;
        private System.Windows.Forms.Label Label_Political;
        private System.Windows.Forms.Label Label_PoliticalRight;
        private System.Windows.Forms.Label Label_PoliticalLeft;
        private System.Windows.Forms.Label Label_SocialLeft;
        private System.Windows.Forms.Label Label_SocialRight;
        private System.Windows.Forms.Label Label_EconomicLeft;
        private System.Windows.Forms.Label Label_EconomicRight;
        private System.Windows.Forms.GroupBox GroupBox_Data;
        public System.Windows.Forms.TreeView TreeView_Notes;
        private System.Windows.Forms.Button Button_LoadSelected;
        private System.Windows.Forms.Button Button_OverwriteSelected;
        private System.Windows.Forms.Button Button_RemoveSelected;
        private System.Windows.Forms.Button Button_RenderSelected1D;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.MenuStrip MenuBar;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_File;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_File_SaveAs;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_File_Open;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_Info;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_Misc_Credits;
        private System.Windows.Forms.ContextMenuStrip TreeView_Notes_CM_LvlOne;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlOne_Create;
        private System.Windows.Forms.ContextMenuStrip TreeView_Notes_CM_LvlTwo;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlOne_Clear;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Edit;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Delete;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Up;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Top;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Down;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Bottom;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Category;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Category_Biographical;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Category_Political;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Category_Economic;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlTwo_Move_Category_Social;
        private System.Windows.Forms.ToolStripMenuItem TreeView_Notes_CM_LvlOne_Parse;
        private System.Windows.Forms.CheckBox CheckBox_Political;
        private System.Windows.Forms.CheckBox CheckBox_Economic;
        private System.Windows.Forms.CheckBox CheckBox_Social;
        private System.Windows.Forms.Button Button_Down;
        private System.Windows.Forms.Button Button_Up;
        private System.Windows.Forms.Button Button_RenderSelected2D;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_File_Save;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_Misc_CheckForUpdates;
        private System.Windows.Forms.RadioButton RadioButton_Ideology;
        private System.Windows.Forms.RadioButton RadioButton_Philosopher;
        private System.Windows.Forms.TrackBar TrackBar_TextSize;
        private System.Windows.Forms.Label Label_TextSize;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_Study;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_Study_Begin;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_Study_AllowClose;
    }
}


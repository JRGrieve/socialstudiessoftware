﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocialStudiesSoftware {
    public partial class Form_Study : Form {
        public static Random Randomness = new Random();
        public static int iCurrentIndex = 0;
        public static List<SocialStudiesSoftware.SocialPerson> ActiveList;
        public static List<TrackBar> TrackBarList;
        public static bool bAllowClose = true;
        public Form_Study(List<SocialStudiesSoftware.SocialPerson> UnshuffledList) {
            InitializeComponent();
            foreach (SocialStudiesSoftware.SocialPerson person in UnshuffledList) Console.Write(person.sName+" - "); Console.Write("\n");
            ActiveList = new List<SocialStudiesSoftware.SocialPerson>(UnshuffledList);
            ShuffleList(ref ActiveList);
            foreach (SocialStudiesSoftware.SocialPerson person in ActiveList) Console.Write(person.sName+" - "); Console.Write("\n");
            Label_Name.Text = ActiveList[0].sName; Console.Write("\n");
            UpdateForm(this, new EventArgs());
        }
        public static void ShuffleList(ref List<SocialStudiesSoftware.SocialPerson> TargetList) {
            List<SocialStudiesSoftware.SocialPerson> TempList = new List<SocialStudiesSoftware.SocialPerson>();
            foreach (SocialStudiesSoftware.SocialPerson Person in TargetList) TempList.Add(Person);
            TargetList.Clear();
            while (TempList.Count > 0) {
                int TargetIndex = Randomness.Next(TempList.Count);
                TargetList.Add(TempList[TargetIndex]);
                TempList.RemoveAt(TargetIndex);
            }
        }
        public static void ResetStudy() {
            MessageBox.Show("We'll do another round with the ones you said you didn't know.", "Round Complete!");
            ShuffleList(ref ActiveList);
            iCurrentIndex = 0;
        }
        public void UpdateForm(object sender, EventArgs e) {
            Button_Submit.Enabled = true;
            Label_Name.Text = ActiveList[iCurrentIndex].sName;
            Button_Yes.Enabled = false;
            Button_No.Enabled = false;
            TrackBar_Political.Value = 0;
            TrackBar_Economic.Value = 0;
            TrackBar_Social.Value = 0;
            if (SocialStudiesSoftware.bConDebug) {
                Console.WriteLine(ActiveList[iCurrentIndex].sName);
                Console.WriteLine("Political Position: "+ActiveList[iCurrentIndex].iPoliticalPosition.ToString());
                Console.WriteLine("Economic Position: "+ActiveList[iCurrentIndex].iEconomicPosition.ToString());
                Console.WriteLine("Social Position: "+ActiveList[iCurrentIndex].iSocialPosition.ToString());
                Console.WriteLine("----------");
            }
            if (ActiveList[iCurrentIndex].iPoliticalPosition == -100) {
                TrackBar_Political.Enabled = false;
                TextBox_Results_Political.Text = "N/A";
            } else {
                TrackBar_Political.Enabled = true;
                TextBox_Results_Political.Text = "";
            }
            if (ActiveList[iCurrentIndex].iEconomicPosition == -100) {
                TrackBar_Economic.Enabled = false;
                TextBox_Results_Economic.Text = "N/A";
            } else {
                TrackBar_Economic.Enabled = true;
                TextBox_Results_Economic.Text = "";
            }
            if (ActiveList[iCurrentIndex].iSocialPosition == -100) {
                TrackBar_Social.Enabled = false;
                TextBox_Results_Social.Text = "N/A";
            } else {
                TrackBar_Social.Enabled = true;
                TextBox_Results_Social.Text = "";
            }
        }
        private void Button_Yes_Click(object sender, EventArgs e) {
            ActiveList.RemoveAt(iCurrentIndex);
            if (ActiveList.Count == 0) {
                Label_Name.Text = "All Finished!";
                Button_Yes.Enabled = false;
                Button_No.Enabled = false;
                TrackBar_Political.Value = 0;
                TrackBar_Economic.Value = 0;
                TrackBar_Social.Value = 0;
                Button_Submit.Enabled = false;
            } else if (ActiveList.Count == iCurrentIndex) {
                ResetStudy();
                UpdateForm(sender, e);
            } else {
                UpdateForm(sender, e);
            }
        }

        private void Button_No_Click(object sender, EventArgs e) {
            iCurrentIndex++;
            if (ActiveList.Count == iCurrentIndex) {
                ResetStudy();
            }
            UpdateForm(sender, e);
        }

        private void Button_Submit_Click(object sender, EventArgs e) {
            Button_Submit.Enabled = false;
            if (TrackBar_Political.Enabled) {
                int iPoliticalResult = TrackBar_Political.Value - ActiveList[iCurrentIndex].iPoliticalPosition;
                switch (iPoliticalResult) {
                    case -2:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Political.Text = "Close. (Right Two More)";
                        } else {
                            TextBox_Results_Political.Text = "Incorrect.";
                        }
                        break;
                    case -1:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Political.Text = "Close. (Right One More)";
                        } else {
                            TextBox_Results_Political.Text = "Incorrect.";
                        }
                        break;
                    case 0:
                        TextBox_Results_Political.Text = "Correct!";
                        break;
                    case 1:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Political.Text = "Close. (Left One More)";
                        } else {
                            TextBox_Results_Political.Text = "Incorrect.";
                        }
                        break;
                    case 2:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Political.Text = "Close. (Left Two More)";
                        } else {
                            TextBox_Results_Political.Text = "Incorrect.";
                        }
                        break;
                    default:
                        TextBox_Results_Political.Text = "Incorrect.";
                        break;
                }
            }
            if (TrackBar_Economic.Enabled) {
                int iEconomicResult = TrackBar_Economic.Value - ActiveList[iCurrentIndex].iEconomicPosition;
                switch (iEconomicResult) {
                    case -2:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Economic.Text = "Close. (Right Two More)";
                        } else {
                            TextBox_Results_Economic.Text = "Incorrect.";
                        }
                        break;
                    case -1:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Economic.Text = "Close. (Right One More)";
                        } else {
                            TextBox_Results_Economic.Text = "Incorrect.";
                        }
                        break;
                    case 0:
                        TextBox_Results_Economic.Text = "Correct!";
                        break;
                    case 1:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Economic.Text = "Close. (Left One More)";
                        } else {
                            TextBox_Results_Economic.Text = "Incorrect.";
                        }
                        break;
                    case 2:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Economic.Text = "Close. (Left Two More)";
                        } else {
                            TextBox_Results_Economic.Text = "Incorrect.";
                        }
                        break;
                    default:
                        TextBox_Results_Political.Text = "Incorrect.";
                        break;
                }
            }
            if (TrackBar_Social.Enabled) {
                int iSocialResult = TrackBar_Social.Value - ActiveList[iCurrentIndex].iSocialPosition;
                switch (iSocialResult) {
                    case -2:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Social.Text = "Close. (Right Two More)";
                        } else {
                            TextBox_Results_Social.Text = "Incorrect.";
                        }
                        break;
                    case -1:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Social.Text = "Close. (Right One More)";
                        } else {
                            TextBox_Results_Social.Text = "Incorrect.";
                        }
                        break;
                    case 0:
                        TextBox_Results_Social.Text = "Correct!";
                        break;
                    case 1:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Social.Text = "Close. (Left One More)";
                        } else {
                            TextBox_Results_Social.Text = "Incorrect.";
                        }
                        break;
                    case 2:
                        if (SocialStudiesSoftware.bStudyAllowClose) {
                            TextBox_Results_Social.Text = "Close. (Left Two More)";
                        } else {
                            TextBox_Results_Social.Text = "Incorrect.";
                        }
                        break;
                    default:
                        TextBox_Results_Political.Text = "Incorrect.";
                        break;
                }
            }
            Button_Yes.Enabled = true;
            Button_No.Enabled = true;
        }

        private void Button_Done_Click(object sender, EventArgs e) {
            this.Close();
        }

    }
}

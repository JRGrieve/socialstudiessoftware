﻿namespace SocialStudiesSoftware {
    partial class Form_Select2DSpectrums {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// Don't tell me what to do.
        private void InitializeComponent() {
            this.CheckBox_Political = new System.Windows.Forms.CheckBox();
            this.CheckBox_Economic = new System.Windows.Forms.CheckBox();
            this.CheckBox_Social = new System.Windows.Forms.CheckBox();
            this.Button_Submit = new System.Windows.Forms.Button();
            this.HiddenString = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CheckBox_Political
            // 
            this.CheckBox_Political.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBox_Political.AutoSize = true;
            this.CheckBox_Political.Checked = true;
            this.CheckBox_Political.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_Political.Location = new System.Drawing.Point(54, 12);
            this.CheckBox_Political.Name = "CheckBox_Political";
            this.CheckBox_Political.Size = new System.Drawing.Size(62, 17);
            this.CheckBox_Political.TabIndex = 0;
            this.CheckBox_Political.Text = "Political";
            this.CheckBox_Political.UseVisualStyleBackColor = true;
            this.CheckBox_Political.CheckedChanged += new System.EventHandler(this.Checkbox_Political_CheckedChanged);
            // 
            // CheckBox_Economic
            // 
            this.CheckBox_Economic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBox_Economic.AutoSize = true;
            this.CheckBox_Economic.Checked = true;
            this.CheckBox_Economic.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_Economic.Location = new System.Drawing.Point(54, 36);
            this.CheckBox_Economic.Name = "CheckBox_Economic";
            this.CheckBox_Economic.Size = new System.Drawing.Size(73, 17);
            this.CheckBox_Economic.TabIndex = 1;
            this.CheckBox_Economic.Text = "Economic";
            this.CheckBox_Economic.UseVisualStyleBackColor = true;
            this.CheckBox_Economic.CheckedChanged += new System.EventHandler(this.Checkbox_Economic_CheckedChanged);
            // 
            // CheckBox_Social
            // 
            this.CheckBox_Social.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBox_Social.AutoSize = true;
            this.CheckBox_Social.Checked = true;
            this.CheckBox_Social.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_Social.Location = new System.Drawing.Point(54, 60);
            this.CheckBox_Social.Name = "CheckBox_Social";
            this.CheckBox_Social.Size = new System.Drawing.Size(55, 17);
            this.CheckBox_Social.TabIndex = 2;
            this.CheckBox_Social.Text = "Social";
            this.CheckBox_Social.UseVisualStyleBackColor = true;
            this.CheckBox_Social.CheckedChanged += new System.EventHandler(this.Checkbox_Social_CheckedChanged);
            // 
            // Button_Submit
            // 
            this.Button_Submit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Submit.Enabled = false;
            this.Button_Submit.Location = new System.Drawing.Point(54, 90);
            this.Button_Submit.Name = "Button_Submit";
            this.Button_Submit.Size = new System.Drawing.Size(75, 23);
            this.Button_Submit.TabIndex = 3;
            this.Button_Submit.Text = "Submit";
            this.Button_Submit.UseVisualStyleBackColor = true;
            this.Button_Submit.Click += new System.EventHandler(this.Button_Submit_Click);
            // 
            // HiddenString
            // 
            this.HiddenString.AutoSize = true;
            this.HiddenString.Location = new System.Drawing.Point(167, 9);
            this.HiddenString.Name = "HiddenString";
            this.HiddenString.Size = new System.Drawing.Size(17, 13);
            this.HiddenString.TabIndex = 4;
            this.HiddenString.Text = "nil";
            this.HiddenString.Visible = false;
            // 
            // Form_Select2DSpectrums
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(192, 125);
            this.Controls.Add(this.HiddenString);
            this.Controls.Add(this.Button_Submit);
            this.Controls.Add(this.CheckBox_Social);
            this.Controls.Add(this.CheckBox_Economic);
            this.Controls.Add(this.CheckBox_Political);
            this.Name = "Form_Select2DSpectrums";
            this.Text = "Select Axes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox CheckBox_Political;
        private System.Windows.Forms.CheckBox CheckBox_Economic;
        private System.Windows.Forms.CheckBox CheckBox_Social;
        private System.Windows.Forms.Button Button_Submit;
        private System.Windows.Forms.Label HiddenString;
    }
}
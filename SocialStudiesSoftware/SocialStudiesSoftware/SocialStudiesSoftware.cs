﻿//-------------------------
//-------------------------
//Social Studies Software
//-------------------------
//By James Grieve
//-------------------------
//-------------------------
//Description:
//An interface and management system for note-taking and studying social philosophers and ideologies.
//-------------------------
//-------------------------

//-LIBRAIRIES-
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Diagnostics;
using GDIDrawer;
//-END LIBRAIRIES-

//-FORM NAMESPACE-
namespace SocialStudiesSoftware {
    //-FORM CLASS-
    public partial class SocialStudiesSoftware : Form {
        //-PREAMBLE FUNCTION-
        static void Preamble() {
            Console.WriteLine("-------------------------");
            Console.WriteLine("-------------------------");
            Console.WriteLine("Program Successfully Initialized.");
            Console.WriteLine("-------------------------");
            Console.WriteLine("-------------------------");
        }
        //-END PREAMBLE FUNCTION-

        //-GLOBAL FORM CLASS VARIABLES-
        WebClient WebParser = new WebClient();
        static BinaryFormatter BinarySerializer = new BinaryFormatter();
        static String sSoftwareVersion = "1.03";
        String sUpdateServer = "http://jamesonrgrieve.site.nfoservers.com/versioning/socialstudies/version.txt";
        public static bool bConDebug = true;
        public static bool bStudyAllowClose = true;
        bool bBypassUpdateCheck = false;


        static List<SocialPerson> SocialPersonList = new List<SocialPerson>();

        String sActiveFile = "";
        Boolean bSavingRequired = false;

        String sNewNote;
        Int16 TempPolitical;
        Int16 TempEconomic;
        Int16 TempSocial;
        TreeNode NewTreeNode;
        TreeNode MovingTreeNode;
        TypeOfEntry TempType;
        SocialPerson NewSocialPerson;
        SocialPerson TempSocialPerson;
        //-END GLOBAL FORM CLASS VARIABLES-

        //-GLOBAL FORM CLASS STRUCTS-
        public enum TypeOfEntry { Philosopher, Ideology };
        
        [Serializable]
        public struct SocialPerson {
            public String sName;
            public String sLatestActiveVersion;
            public Int16 iPoliticalPosition;
            public Int16 iEconomicPosition;
            public Int16 iSocialPosition;
            public List<String> BiographicalNotesList;
            public List<String> PoliticalNotesList;
            public List<String> EconomicNotesList;
            public List<String> SocialNotesList;
            public TypeOfEntry EntryType;

            public SocialPerson(String sNameArg, Int16 iPoliticalPositionArg, Int16 iEconomicPositionArg, Int16 iSocialPositionArg, TypeOfEntry EntryTypeArg) {
                sName = sNameArg;
                sLatestActiveVersion = sSoftwareVersion;
                iPoliticalPosition = iPoliticalPositionArg;
                iEconomicPosition = iEconomicPositionArg;
                iSocialPosition = iSocialPositionArg;
                BiographicalNotesList = new List<String>();
                PoliticalNotesList = new List<String>();
                EconomicNotesList = new List<String>();
                SocialNotesList = new List<String>();
                EntryType = EntryTypeArg;

            }
        }
        //-END GLOBAL FORM CLASS STRUCTS-

        //-FORM CLASS FUNCTIONS-
        public SocialStudiesSoftware() {
            /*/
            Parameters: 
                N/A
            Returns:
                N/A
            Notes:
                Form initialization function.
            /*/

            InitializeComponent();
            Preamble();
            // Print the preamble to console establishing that the form has been initialized.

            Text = Text + " [Version "+sSoftwareVersion+"]";
            // Dynamically append the software version to the form's title.

            if (!bBypassUpdateCheck) {
                if (bConDebug) Console.WriteLine("Downloading latest version number from server: " + sUpdateServer);
                // [Debug Only] Print the update server being used, just in case something weird happens.
                string sUpdateString = WebParser.DownloadString(sUpdateServer);
                // Download the content of the file designated in global variabled and store it in a string.
                Boolean bUpdateRequired = Double.Parse(sUpdateString) > Double.Parse(sSoftwareVersion);
                // Compare the current version number to the retrieved one, and store the result for the next few lines.
                if (bConDebug) {
                    Console.WriteLine("Current Version Number: " + sSoftwareVersion);
                    Console.WriteLine("Retrieved Version Number: " + sUpdateString);
                    if (bUpdateRequired) { Console.WriteLine("Detected latest version from server is higher than current version. Notifying user."); } else { Console.WriteLine("Detected current version is equivalent (or higher in case of development) to latest version. Continuing with startup."); }
                    Console.WriteLine("-------------------------");
                }
                // [Debug Only] Print the current and retreived version number, and the result of the comparison.
                if (bUpdateRequired) MessageBox.Show("There is a new version available. If you would like to update, you can select the 'Check For Updates' option under 'Misc'.", "Update Notification");
                // If the current version number is below the retrieved one, notify the user of update procedure.
            }

            TreeView_Notes.Nodes[0].ExpandAll();
            // Expand the treeview so the user can see all of the nodes.
        }
        private void Button_CreateNew_Click(object sender, EventArgs e) {
            if (TextBox_Name.Text == "") {
                MessageBox.Show("You must enter a name for your new entry.", "Error");
            } else if (RadioButton_Philosopher.Checked == RadioButton_Ideology.Checked) {
                MessageBox.Show("You must choose either Philosopher or Ideology.", "Error");
            } else {
                TempPolitical = -100;
                TempEconomic = -100;
                TempSocial = -100;
                if (CheckBox_Political.Checked) {
                    TempPolitical = Convert.ToInt16(TrackBar_Political.Value);
                }
                if (CheckBox_Economic.Checked) {
                    TempEconomic = Convert.ToInt16(TrackBar_Economic.Value);
                }
                if (CheckBox_Social.Checked) {
                    TempSocial = Convert.ToInt16(TrackBar_Social.Value);
                }
                //Console.WriteLine("Saving as: "+TempPolitical+" "+TempEconomic+" "+TempSocial);
                if (RadioButton_Ideology.Checked) {
                    TempType = TypeOfEntry.Ideology;
                } else if (RadioButton_Philosopher.Checked) {
                    TempType = TypeOfEntry.Philosopher;
                }
                NewSocialPerson = new SocialPerson(TextBox_Name.Text, TempPolitical, TempEconomic, TempSocial, TempType);
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[0].Nodes.Count; i++) {
                    NewSocialPerson.BiographicalNotesList.Add(TreeView_Notes.Nodes[0].Nodes[0].Nodes[i].Text);
                }
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[1].Nodes.Count; i++) {
                    NewSocialPerson.PoliticalNotesList.Add(TreeView_Notes.Nodes[0].Nodes[1].Nodes[i].Text);
                }
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[2].Nodes.Count; i++) {
                    NewSocialPerson.EconomicNotesList.Add(TreeView_Notes.Nodes[0].Nodes[2].Nodes[i].Text);
                }
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[3].Nodes.Count; i++) {
                    NewSocialPerson.SocialNotesList.Add(TreeView_Notes.Nodes[0].Nodes[3].Nodes[i].Text);
                }
                SocialPersonList.Add(NewSocialPerson);
                if (RadioButton_Ideology.Checked) {
                    ListBox_SocialPeople.Items.Add("[I] "+TextBox_Name.Text);
                } else if (RadioButton_Philosopher.Checked) {
                    ListBox_SocialPeople.Items.Add("[P] "+TextBox_Name.Text);
                }
                bSavingRequired = true;
            }
        }
        private void Button_OverwriteSelected_Click(object sender, EventArgs e) {
            if (TextBox_Name.Text == "") {
                MessageBox.Show("You must enter a name in order to overwrite this entry.", "Error");
            } else if (ListBox_SocialPeople.SelectedItem != null) {
                TempPolitical = -100;
                TempEconomic = -100;
                TempSocial = -100;
                if (CheckBox_Political.Checked) {
                    TempPolitical = Convert.ToInt16(TrackBar_Political.Value);
                } 
                if (CheckBox_Economic.Checked) {
                    TempEconomic = Convert.ToInt16(TrackBar_Economic.Value);
                }
                if (CheckBox_Social.Checked) {
                    TempSocial = Convert.ToInt16(TrackBar_Social.Value);
                }
                //Console.WriteLine("Saving as: "+TempPolitical+" "+TempEconomic+" "+TempSocial);
                if (RadioButton_Ideology.Checked) {
                    TempType = TypeOfEntry.Ideology;
                } else if (RadioButton_Philosopher.Checked) {
                    TempType = TypeOfEntry.Philosopher;
                }
                NewSocialPerson = new SocialPerson(TextBox_Name.Text, TempPolitical, TempEconomic, TempSocial, TempType);
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[0].Nodes.Count; i++) {
                    NewSocialPerson.BiographicalNotesList.Add(TreeView_Notes.Nodes[0].Nodes[0].Nodes[i].Text);
                }
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[1].Nodes.Count; i++) {
                    NewSocialPerson.PoliticalNotesList.Add(TreeView_Notes.Nodes[0].Nodes[1].Nodes[i].Text);
                }
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[2].Nodes.Count; i++) {
                    NewSocialPerson.EconomicNotesList.Add(TreeView_Notes.Nodes[0].Nodes[2].Nodes[i].Text);
                }
                for (int i = 0; i < TreeView_Notes.Nodes[0].Nodes[3].Nodes.Count; i++) {
                    NewSocialPerson.SocialNotesList.Add(TreeView_Notes.Nodes[0].Nodes[3].Nodes[i].Text);
                }
                SocialPersonList[ListBox_SocialPeople.SelectedIndex] = NewSocialPerson;
                if (RadioButton_Ideology.Checked) {
                    ListBox_SocialPeople.Items[ListBox_SocialPeople.SelectedIndex] = "[I] "+TextBox_Name.Text;
                } else if (RadioButton_Philosopher.Checked) {
                    ListBox_SocialPeople.Items[ListBox_SocialPeople.SelectedIndex] = "[P] "+TextBox_Name.Text;
                }
                bSavingRequired = true;
            }
        }
        private void Button_LoadSelected_Click(object sender, EventArgs e) {
            if (ListBox_SocialPeople.SelectedItem != null) {
                TextBox_Name.Text = SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName;
                //Console.WriteLine("Loading: "+SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition+" "+SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition+" "+SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition+SocialPersonList[ListBox_SocialPeople.SelectedIndex].EntryType);
                if (Convert.ToInt32(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition) == -100) {
                    TrackBar_Political.Enabled = true;
                    TrackBar_Political.Value = 0;
                    TrackBar_Political.Enabled = false;
                    Label_PoliticalLeft.Enabled = false;
                    Label_PoliticalRight.Enabled = false;
                    CheckBox_Political.Checked = false;
                } else {
                    CheckBox_Political.Checked = true;
                    TrackBar_Political.Enabled = true;
                    Label_PoliticalLeft.Enabled = true;
                    Label_PoliticalRight.Enabled = true;
                    TrackBar_Political.Value = Convert.ToInt32(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition);
                }
                if (Convert.ToInt32(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition) == -100) {
                    TrackBar_Economic.Enabled = true;
                    TrackBar_Economic.Value = 0;
                    TrackBar_Economic.Enabled = false;
                    Label_EconomicLeft.Enabled = false;
                    Label_EconomicRight.Enabled = false;
                    CheckBox_Economic.Checked = false;
                } else {
                    CheckBox_Economic.Checked = true;
                    TrackBar_Economic.Enabled = true;
                    Label_EconomicLeft.Enabled = true;
                    Label_EconomicRight.Enabled = true;
                    TrackBar_Economic.Value = Convert.ToInt32(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition);
                }
                if (Convert.ToInt32(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition) == -100) {
                    TrackBar_Social.Enabled = true;
                    TrackBar_Social.Value = 0;
                    TrackBar_Social.Enabled = false;
                    Label_SocialLeft.Enabled = false;
                    Label_SocialRight.Enabled = false;
                    CheckBox_Social.Checked = false;
                } else {
                    CheckBox_Social.Checked = true;
                    TrackBar_Social.Enabled = true;
                    Label_SocialLeft.Enabled = true;
                    Label_SocialRight.Enabled = true;
                    TrackBar_Social.Value = Convert.ToInt32(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition);
                }
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].EntryType == TypeOfEntry.Ideology) {
                    RadioButton_Ideology.Checked = true;
                }
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].EntryType == TypeOfEntry.Philosopher) {
                    RadioButton_Philosopher.Checked = true;
                }
                TreeView_Notes.Nodes[0].Nodes[0].Nodes.Clear();
                TreeView_Notes.Nodes[0].Nodes[1].Nodes.Clear();
                TreeView_Notes.Nodes[0].Nodes[2].Nodes.Clear();
                TreeView_Notes.Nodes[0].Nodes[3].Nodes.Clear();
                for (int i = 0; i < SocialPersonList[ListBox_SocialPeople.SelectedIndex].BiographicalNotesList.Count; i++) {
                    NewTreeNode = new TreeNode(SocialPersonList[ListBox_SocialPeople.SelectedIndex].BiographicalNotesList[i]);
                    TreeView_Notes.Nodes[0].Nodes[0].Nodes.Add(NewTreeNode);
                }
                for (int i = 0; i < SocialPersonList[ListBox_SocialPeople.SelectedIndex].PoliticalNotesList.Count; i++) {
                    NewTreeNode = new TreeNode(SocialPersonList[ListBox_SocialPeople.SelectedIndex].PoliticalNotesList[i]);
                    TreeView_Notes.Nodes[0].Nodes[1].Nodes.Add(NewTreeNode);
                }
                for (int i = 0; i < SocialPersonList[ListBox_SocialPeople.SelectedIndex].EconomicNotesList.Count; i++) {
                    NewTreeNode = new TreeNode(SocialPersonList[ListBox_SocialPeople.SelectedIndex].EconomicNotesList[i]);
                    TreeView_Notes.Nodes[0].Nodes[2].Nodes.Add(NewTreeNode);
                }
                for (int i = 0; i < SocialPersonList[ListBox_SocialPeople.SelectedIndex].SocialNotesList.Count; i++) {
                    NewTreeNode = new TreeNode(SocialPersonList[ListBox_SocialPeople.SelectedIndex].SocialNotesList[i]);
                    TreeView_Notes.Nodes[0].Nodes[3].Nodes.Add(NewTreeNode);
                }
            }
        }
        public void ListBox_SavedPeople_MoveItem(int direction) {
            // Check selected item.
            if (ListBox_SocialPeople.SelectedItem == null || ListBox_SocialPeople.SelectedIndex < 0)
                return; // No selected item, do nothing.

            // Calculate new index using move direction.
            int newIndex = ListBox_SocialPeople.SelectedIndex + direction;

            // Check bounds of the range.
            if (newIndex < 0 || newIndex >= ListBox_SocialPeople.Items.Count)
                return; // Index out of range, do nothing.

            int selIndex = ListBox_SocialPeople.SelectedIndex;
            object selected = ListBox_SocialPeople.SelectedItem;

            // Remove removable element.
            NewSocialPerson = SocialPersonList[selIndex];
            SocialPersonList.RemoveAt(selIndex);
            ListBox_SocialPeople.Items.Remove(selected);
            // Insert it in new position.
            SocialPersonList.Insert(newIndex, NewSocialPerson);
            ListBox_SocialPeople.Items.Insert(newIndex, selected);
            // Restore selection.
            ListBox_SocialPeople.SetSelected(newIndex, true);
        }
        private void Button_Up_Click(object sender, EventArgs e) {
            ListBox_SavedPeople_MoveItem(-1);
            bSavingRequired = true;
        }
        private void Button_Down_Click(object sender, EventArgs e) {
            ListBox_SavedPeople_MoveItem(1);
            bSavingRequired = true;
        }
        private void Button_RemoveSelected_Click(object sender, EventArgs e) {
            if (ListBox_SocialPeople.SelectedItem != null) {
                SocialPersonList.RemoveAt(ListBox_SocialPeople.SelectedIndex);
                ListBox_SocialPeople.Items.RemoveAt(ListBox_SocialPeople.SelectedIndex);
                bSavingRequired = true;
                if (ListBox_SocialPeople.Items.Count == 0) bSavingRequired = false;
            }
        }
        private void CheckBox_Political_CheckedChanged(object sender, EventArgs e) {
            if (CheckBox_Political.Checked) {
                TrackBar_Political.Enabled = true;
                Label_PoliticalLeft.Enabled = true;
                Label_PoliticalRight.Enabled = true;
            } else {
                TrackBar_Political.Enabled = true;
                TrackBar_Political.Value = 0;
                TrackBar_Political.Enabled = false;
                Label_PoliticalLeft.Enabled = false;
                Label_PoliticalRight.Enabled = false;
            }
        }
        private void CheckBox_Economic_CheckedChanged(object sender, EventArgs e) {
            if (CheckBox_Economic.Checked) {
                TrackBar_Economic.Enabled = true;
                Label_EconomicLeft.Enabled = true;
                Label_EconomicRight.Enabled = true;
            } else {
                TrackBar_Economic.Enabled = true;
                TrackBar_Economic.Value = 0;
                TrackBar_Economic.Enabled = false;
                Label_EconomicLeft.Enabled = false;
                Label_EconomicRight.Enabled = false;
            }
        }
        private void CheckBox_Social_CheckedChanged(object sender, EventArgs e) {
            if (CheckBox_Social.Checked) {
                TrackBar_Social.Enabled = true;
                Label_SocialLeft.Enabled = true;
                Label_SocialRight.Enabled = true;
            } else {
                TrackBar_Social.Enabled = true;
                TrackBar_Social.Value = 0;
                TrackBar_Social.Enabled = false;
                Label_SocialLeft.Enabled = false;
                Label_SocialRight.Enabled = false;
            }
        }
        private void RenderSpectrum(ref CDrawer RenderWindowRef, int iLeftMargin, int iRightMargin, int iSpectrumHeight, Color cSpectrumColor) {
            RenderWindowRef.AddLine(iLeftMargin, iSpectrumHeight, iRightMargin, iSpectrumHeight, Color.Black, 8); //Main Line Outline
            RenderWindowRef.AddLine(iLeftMargin, iSpectrumHeight, iLeftMargin+25, iSpectrumHeight-25, Color.Black, 8); //Left Arrow Top Outline
            RenderWindowRef.AddLine(iLeftMargin, iSpectrumHeight, iLeftMargin+25, iSpectrumHeight+25, Color.Black, 8); //Left Arrow Bottom Outline
            RenderWindowRef.AddLine(iRightMargin, iSpectrumHeight, iRightMargin-25, iSpectrumHeight-25, Color.Black, 8); //Right Arrow Top Outline
            RenderWindowRef.AddLine(iRightMargin, iSpectrumHeight, iRightMargin-25, iSpectrumHeight+25, Color.Black, 8); //Right Arrow Bottom Outline
            RenderWindowRef.AddLine((iLeftMargin+iRightMargin)/2, iSpectrumHeight-15, (iLeftMargin+iRightMargin)/2, iSpectrumHeight+15, Color.Black, 8); //Center Line Outline

            RenderWindowRef.AddLine(iLeftMargin, iSpectrumHeight, iRightMargin, iSpectrumHeight, cSpectrumColor, 5); //Main Line  
            RenderWindowRef.AddLine(iLeftMargin, iSpectrumHeight, iLeftMargin+25, iSpectrumHeight-25, cSpectrumColor, 5); //Left Arrow Top 
            RenderWindowRef.AddLine(iLeftMargin, iSpectrumHeight, iLeftMargin+25, iSpectrumHeight+25, cSpectrumColor, 5); //Left Arrow Bottom 
            RenderWindowRef.AddLine(iRightMargin, iSpectrumHeight, iRightMargin-25, iSpectrumHeight-25, cSpectrumColor, 5); //Right Arrow Top 
            RenderWindowRef.AddLine(iRightMargin, iSpectrumHeight, iRightMargin-25, iSpectrumHeight+25, cSpectrumColor, 5); //Right Arrow Bottom 
            RenderWindowRef.AddLine((iLeftMargin+iRightMargin)/2, iSpectrumHeight-15, (iLeftMargin+iRightMargin)/2, iSpectrumHeight+15, cSpectrumColor, 5); //Center Line 
        }
        private void RenderGrid(ref CDrawer RenderWindowRef, int iLeftMargin, int iRightMargin, int iTopMargin, int iBottomMargin) {
            RenderWindowRef.AddLine(iLeftMargin, iTopMargin, iLeftMargin, iBottomMargin, Color.Black, 8); //Left Wall
            RenderWindowRef.AddLine(iRightMargin, iTopMargin, iRightMargin, iBottomMargin, Color.Black, 8); //Right Wall
            RenderWindowRef.AddLine(iLeftMargin, iTopMargin, iRightMargin, iTopMargin, Color.Black, 8); //Roof
            RenderWindowRef.AddLine(iLeftMargin, iBottomMargin, iRightMargin, iBottomMargin, Color.Black, 8); //Floor

            for (Int32 i = -250; i <= 250; i += 50) {
                if (i == 0) {
                    RenderWindowRef.AddLine(iLeftMargin+3, ((iBottomMargin-iTopMargin)/2)+iTopMargin, iRightMargin-3, ((iBottomMargin-iTopMargin)/2)+iTopMargin, Color.Black, 2); //X=-5
                    RenderWindowRef.AddLine((((iRightMargin-iLeftMargin)/2)+iLeftMargin), iTopMargin+3, ((iRightMargin-iLeftMargin)/2)+iLeftMargin, iBottomMargin-3, Color.Black, 2);
                } else {
                    RenderWindowRef.AddLine(iLeftMargin+3, ((iBottomMargin-iTopMargin)/2)+iTopMargin-i, iRightMargin-3, ((iBottomMargin-iTopMargin)/2)+iTopMargin-i, Color.Gray, 2); //X=-5
                    RenderWindowRef.AddLine((((iRightMargin-iLeftMargin)/2)+iLeftMargin)-i, iTopMargin+3, ((iRightMargin-iLeftMargin)/2)+iLeftMargin-i, iBottomMargin-3, Color.Gray, 2);
                }
            }
        }
        private void RenderMarker(ref CDrawer RenderWindowRef, int iMarkerX, int iSpectrumHeight, Color cMarkerColor) {
            RenderWindowRef.AddLine(iMarkerX, iSpectrumHeight-15, iMarkerX, iSpectrumHeight+15, Color.Black, 10); //Marker Outline
            RenderWindowRef.AddLine(iMarkerX, iSpectrumHeight-15, iMarkerX, iSpectrumHeight+15, cMarkerColor, 5); //Marker Line 
        }
        private int AddFittedText(ref CDrawer RenderWindowRef, String sText, float iSize, int iTopLeftX, int iTopLeftY, int iWidth, int iHeight, Color cColor) {
            String[] sWords = sText.Split(new char[] {' '});
            List<String> sLines = new List<String>();
            int iCharNum = (int)Math.Round(iWidth / (iSize/1.48), 0);
            sLines.Add("");
            for (Int32 i = 0; i < sWords.Length; i++) {
                if ((i < sWords.Length-1) && (sLines[sLines.Count-1].Length + sWords[i].Length + sWords[i+1].Length <= iCharNum)) {
                    sLines[sLines.Count-1] += sWords[i];
                    sLines[sLines.Count-1] += " ";
                } else if (i == sWords.Length-1) {
                    sLines[sLines.Count-1] += sWords[i];
                } else {
                    sLines[sLines.Count-1] += sWords[i];
                    sLines.Add("");                
                }
            }
            int iWorkingY = iTopLeftY;
            foreach (String sToAdd in sLines) {
                RenderWindowRef.AddText(sToAdd, iSize, iTopLeftX, iWorkingY, iWidth, iHeight, cColor);
                iWorkingY += (int)Math.Round(iSize*1.2, 0);
            }

            return iWorkingY;
        }
        private void Button_RenderSelected1D_Click(object sender, EventArgs e) {
            if (ListBox_SocialPeople.SelectedItem != null) {
                CDrawer RenderWindow = new CDrawer(640, 740);
                int iLeftMargin = 20;
                int iRightMargin = 620;
                Color cPoliticalColor = Color.DarkRed;
                Color cEconomicColor = Color.Green;
                Color cSocialColor = Color.Blue;
                RenderWindow.AddRectangle(5, 5, 630, 730, Color.White);
                RenderWindow.AddText(SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName, 20, 20, 20, 600, 40, Color.Black);
                RenderWindow.AddLine(((iLeftMargin+iRightMargin)/2)-(SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName.Length*8), 56, ((iLeftMargin+iRightMargin)/2)+(SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName.Length*8), 56, Color.Black, 2); //Underline
                RenderWindow.AddLine(((iLeftMargin+iRightMargin)/2)-(SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName.Length*8), 60, ((iLeftMargin+iRightMargin)/2)+(SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName.Length*8), 60, Color.Black, 2); //Underline

                int iCurrentHeight = 145;

                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition != -100) {
                    int iPoliticalHeight = iCurrentHeight;
                    RenderWindow.AddText("Political Stance", 16, iLeftMargin, iPoliticalHeight-65, 600, 40, Color.Black);
                    RenderWindow.AddText("Democracy", 7+TrackBar_TextSize.Value, iLeftMargin-15, iPoliticalHeight-10, 70, 20, Color.Black);
                    RenderWindow.AddText("Dictatorship", 7+TrackBar_TextSize.Value, iRightMargin-55, iPoliticalHeight-10, 70, 20, Color.Black);
                    RenderWindow.AddLine(((iLeftMargin+iRightMargin)/2)-50, iPoliticalHeight-30, ((iLeftMargin+iRightMargin)/2)+50, iPoliticalHeight-30, Color.Black, 2); //Underline
                    RenderSpectrum(ref RenderWindow, iLeftMargin+60, iRightMargin-60, iPoliticalHeight, cPoliticalColor);
                    RenderMarker(ref RenderWindow, ((iLeftMargin+iRightMargin)/2)-(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition*(-43)), iPoliticalHeight, Color.Yellow);
                    iCurrentHeight += 90;
                }

                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition != -100) {
                    int iEconomicHeight = iCurrentHeight;
                    RenderWindow.AddText("Economic Stance", 16, iLeftMargin, iEconomicHeight-65, 600, 40, Color.Black);
                    RenderWindow.AddText("Planned", 7+TrackBar_TextSize.Value, iLeftMargin-15, iEconomicHeight-10, 70, 20, Color.Black);
                    RenderWindow.AddText("Capitalist", 7+TrackBar_TextSize.Value, iRightMargin-55, iEconomicHeight-10, 70, 20, Color.Black);
                    RenderWindow.AddLine(((iLeftMargin+iRightMargin)/2)-50, iEconomicHeight-30, ((iLeftMargin+iRightMargin)/2)+50, iEconomicHeight-30, Color.Black, 2); //Underline
                    RenderSpectrum(ref RenderWindow, iLeftMargin+60, iRightMargin-60, iEconomicHeight, cEconomicColor);
                    RenderMarker(ref RenderWindow, ((iLeftMargin+iRightMargin)/2)-(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition*(-43)), iEconomicHeight, Color.Yellow);
                    iCurrentHeight += 90;
                }

                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition != -100) {
                    int iSocialHeight = iCurrentHeight;
                    RenderWindow.AddText("Social Stance", 16, iLeftMargin, iSocialHeight-65, 600, 40, Color.Black);
                    RenderWindow.AddText("Collectivism", 7+TrackBar_TextSize.Value, iLeftMargin-15, iSocialHeight-10, 70, 20, Color.Black);
                    RenderWindow.AddText("Individualism", 7+TrackBar_TextSize.Value, iRightMargin-55, iSocialHeight-10, 70, 20, Color.Black);
                    RenderWindow.AddLine(((iLeftMargin+iRightMargin)/2)-50, iSocialHeight-30, ((iLeftMargin+iRightMargin)/2)+50, iSocialHeight-30, Color.Black, 2); //Underline
                    RenderSpectrum(ref RenderWindow, iLeftMargin+60, iRightMargin-60, iSocialHeight, cSocialColor);
                    RenderMarker(ref RenderWindow, ((iLeftMargin+iRightMargin)/2)-(SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition*(-43)), iSocialHeight, Color.Yellow);
                    iCurrentHeight += 90;
                }

                iCurrentHeight -= 15;
                RenderWindow.AddText("NOTES", 16, 20, iCurrentHeight, 600, 40, Color.Black);
                iCurrentHeight += 33;
                RenderWindow.AddLine(290, iCurrentHeight, 350, iCurrentHeight, Color.Black, 2); //Underline
                iCurrentHeight -= 3;

                float fTextSize = (float)8;
                foreach (string sBNote in SocialPersonList[ListBox_SocialPeople.SelectedIndex].BiographicalNotesList) {
                    if (sBNote != "None.") {
                        iCurrentHeight = AddFittedText(ref RenderWindow, sBNote.Substring(2, sBNote.Length-2), fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                        iCurrentHeight += 10;
                    }
                }
                foreach (string sPNote in SocialPersonList[ListBox_SocialPeople.SelectedIndex].PoliticalNotesList) {
                    if (sPNote != "None.") {
                        iCurrentHeight = AddFittedText(ref RenderWindow, sPNote.Substring(2, sPNote.Length-2), fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                        iCurrentHeight += 10;
                    }
                }
                foreach (string sENote in SocialPersonList[ListBox_SocialPeople.SelectedIndex].EconomicNotesList) {
                    if (sENote != "None.") {
                        iCurrentHeight = AddFittedText(ref RenderWindow, sENote.Substring(2, sENote.Length-2), fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                        iCurrentHeight += 10;
                    }
                }
                foreach (string sSNote in SocialPersonList[ListBox_SocialPeople.SelectedIndex].SocialNotesList) {
                    if (sSNote != "None.") {
                        iCurrentHeight = AddFittedText(ref RenderWindow, sSNote.Substring(2, sSNote.Length-2), fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                        iCurrentHeight += 10;
                    }
                }
                List<SocialPerson> SimilarPoliticalPeople = new List<SocialPerson>();
                List<SocialPerson> SimilarEconomicPeople = new List<SocialPerson>();
                List<SocialPerson> SimilarSocialPeople = new List<SocialPerson>();
                List<SocialPerson> SimilarPeople = new List<SocialPerson>();
                foreach (SocialPerson ThisPerson in SocialPersonList) {
                    if ((SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition != -100) &&
                        (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition - ThisPerson.iPoliticalPosition >= -1) && 
                        (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition - ThisPerson.iPoliticalPosition <= 1) &&
                        !(ThisPerson.sName == SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName)
                        ) {
                        SimilarPoliticalPeople.Add(ThisPerson);
                    }
                    if ((SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition != -100) &&
                        (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition - ThisPerson.iEconomicPosition >= -1) && 
                        (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition - ThisPerson.iEconomicPosition <= 1) &&
                        !(ThisPerson.sName == SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName)
                        ) {
                        SimilarEconomicPeople.Add(ThisPerson);
                    }

                    if ((SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition != -100) &&
                        (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition - ThisPerson.iSocialPosition >= -1) && 
                        (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition - ThisPerson.iSocialPosition <= 1) &&
                        !(ThisPerson.sName == SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName)
                        ) {
                        SimilarSocialPeople.Add(ThisPerson);
                    }
                }
                List<String> RelevantAreas = new List<String>();
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition != -100) RelevantAreas.Add("Political");
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition != -100) RelevantAreas.Add("Economic");
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition != -100) RelevantAreas.Add("Social");

                if (RelevantAreas.Count == 3) {
                    foreach (SocialPerson ThisPerson in SimilarPoliticalPeople) {
                        if (SimilarEconomicPeople.Contains(ThisPerson)) {
                            if (SimilarSocialPeople.Contains(ThisPerson)) {
                                SimilarPeople.Add(ThisPerson);
                            }
                        }
                    }
                } else if ((RelevantAreas.Count == 2) && (RelevantAreas.Contains("Political")) && (RelevantAreas.Contains("Economic"))) {
                    foreach (SocialPerson ThisPerson in SimilarPoliticalPeople) {
                        if (SimilarEconomicPeople.Contains(ThisPerson)) {
                            SimilarPeople.Add(ThisPerson);
                        }
                    }
                } else if ((RelevantAreas.Count == 2) && (RelevantAreas.Contains("Political")) && (RelevantAreas.Contains("Social"))) {
                    foreach (SocialPerson ThisPerson in SimilarPoliticalPeople) {
                        if (SimilarSocialPeople.Contains(ThisPerson)) {
                            SimilarPeople.Add(ThisPerson);
                        }
                    }
                } else if ((RelevantAreas.Count == 2) && (RelevantAreas.Contains("Economic")) && (RelevantAreas.Contains("Social"))) {
                    foreach (SocialPerson ThisPerson in SimilarEconomicPeople) {
                        if (SimilarSocialPeople.Contains(ThisPerson)) {
                            SimilarPeople.Add(ThisPerson);
                        }
                    }
                }
                foreach (SocialPerson ThisPerson in SimilarPeople) {
                    try { SimilarPoliticalPeople.Remove(ThisPerson); } catch {}
                    try { SimilarEconomicPeople.Remove(ThisPerson); } catch {}
                    try { SimilarSocialPeople.Remove(ThisPerson); } catch {}
                }
                String sSimilarPolitical = "Similar Political Stance: ";
                String sSimilarEconomic = "Similar Economic Stance: ";
                String sSimilarSocial = "Similar Social Stance: ";
                String sSimilarAll = "Similar Stance In All Enabled Areas: ";
                if (SimilarPoliticalPeople.Count == 0) {
                    sSimilarPolitical += "None.\n";
                } else {
                    for (Int32 i = 0; i < SimilarPoliticalPeople.Count; i++) {
                        if (i == SimilarPoliticalPeople.Count-1) {
                            sSimilarPolitical += SimilarPoliticalPeople[i].sName+".\n";
                        } else {
                            sSimilarPolitical += SimilarPoliticalPeople[i].sName+", ";
                        }
                    }
                }
                if (SimilarEconomicPeople.Count == 0) {
                    sSimilarEconomic += "None.\n";
                } else {
                    for (Int32 i = 0; i < SimilarEconomicPeople.Count; i++) {
                        if (i == SimilarEconomicPeople.Count-1) {
                            sSimilarEconomic += SimilarEconomicPeople[i].sName+".\n";
                        } else {
                            sSimilarEconomic += SimilarEconomicPeople[i].sName+", ";
                        }
                    }
                }
                if (SimilarSocialPeople.Count == 0) {
                    sSimilarSocial += "None.\n";
                } else {
                    for (Int32 i = 0; i < SimilarSocialPeople.Count; i++) {
                        if (i == SimilarSocialPeople.Count-1) {
                            sSimilarSocial += SimilarSocialPeople[i].sName+".\n";
                        } else {
                            sSimilarSocial += SimilarSocialPeople[i].sName+", ";
                        }
                    }
                }
                if (SimilarPeople.Count == 0) {
                    sSimilarAll += "None.\n";
                } else {
                    for (Int32 i = 0; i < SimilarPeople.Count; i++) {
                        if (i == SimilarPeople.Count-1) {
                            sSimilarAll += SimilarPeople[i].sName+".\n";
                        } else {
                            sSimilarAll += SimilarPeople[i].sName+", ";
                        }
                    }
                }
                iCurrentHeight += 30;
                RenderWindow.AddText("COMPARISONS", 16, 20, iCurrentHeight, 600, 40, Color.Black);
                iCurrentHeight += 33;
                RenderWindow.AddLine(270, iCurrentHeight, 370, iCurrentHeight, Color.Black, 2); //Underline
                iCurrentHeight -= 8;
                iCurrentHeight = AddFittedText(ref RenderWindow, "Note: Be careful when drawing conclusions from these comparisons. Even though one stance may be similar, the entries' other beliefs may make their overall worldview very different.", fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                iCurrentHeight += 10
                    ;
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition != -100) {
                    iCurrentHeight = AddFittedText(ref RenderWindow, sSimilarPolitical, fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                    iCurrentHeight += 10;
                }
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition != -100) {
                    iCurrentHeight = AddFittedText(ref RenderWindow, sSimilarEconomic, fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                    iCurrentHeight += 10;
                }
                if (SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition != -100) {
                    iCurrentHeight = AddFittedText(ref RenderWindow, sSimilarSocial, fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                    iCurrentHeight += 10;
                }
                if (RelevantAreas.Count != 1) {
                    iCurrentHeight = AddFittedText(ref RenderWindow, sSimilarAll, fTextSize+TrackBar_TextSize.Value, 20, iCurrentHeight, 600, 40, Color.Black);
                    iCurrentHeight += 10;
                }        
            }
        }
        private void Button_RenderSelected2D_Click(object sender, EventArgs e) {
            if (ListBox_SocialPeople.SelectedItem != null) {
                List<Int32> Spectrums = Select2DSpectrumsForm.ShowDialog(SocialPersonList[ListBox_SocialPeople.SelectedIndex]);
                if (Spectrums.Count == 2) {
                    CDrawer RenderWindow = new CDrawer(740, 740);
                    string XAxisLabel = "";
                    string YAxisLabel = "";
                    int iLeftMargin = 120;
                    int iRightMargin = 670;
                    int iTopMargin = 120;
                    int iBottomMargin = 670;
                    int iXPos = 0;
                    int iYPos = 0;
                    RenderWindow.AddRectangle(5, 5, 730, 730, Color.White);
                    RenderGrid(ref RenderWindow, iLeftMargin, iRightMargin, iTopMargin, iBottomMargin);
                    if (Spectrums[0] == 1) {
                        XAxisLabel = "Political";
                    } else if (Spectrums[0] == 2) {
                        XAxisLabel = "Economic";
                    }
                    if (Spectrums[1] == 2) {
                        YAxisLabel = "E\nc\no\nn\no\nm\ni\nc";
                    } else if (Spectrums[1] == 3) {
                        YAxisLabel = "S\no\nc\ni\na\nl";
                    }
                    RenderWindow.AddText(XAxisLabel, 14+TrackBar_TextSize.Value, iLeftMargin, 10, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                    RenderWindow.AddText(YAxisLabel, 14+TrackBar_TextSize.Value, 10, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                    if (XAxisLabel == "Political") {
                        iYPos = SocialPersonList[ListBox_SocialPeople.SelectedIndex].iPoliticalPosition;
                        RenderWindow.AddText("Democracy", 14+TrackBar_TextSize.Value, iLeftMargin, 70, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                        RenderWindow.AddText("Dictatorship", 14+TrackBar_TextSize.Value, iLeftMargin, 670, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                    } else if (XAxisLabel == "Economic") {
                        iYPos = SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition;
                        RenderWindow.AddText("Centrally", 14+TrackBar_TextSize.Value, iLeftMargin, 50, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                        RenderWindow.AddText("Planned", 14+TrackBar_TextSize.Value, iLeftMargin, 70, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                        RenderWindow.AddText("Free", 14+TrackBar_TextSize.Value, iLeftMargin, 670, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                        RenderWindow.AddText("Enterprise", 14+TrackBar_TextSize.Value, iLeftMargin, 690, (iRightMargin)-(iLeftMargin), 50, Color.Black);
                    }
                    if (YAxisLabel == "E\nc\no\nn\no\nm\ni\nc") {
                        iXPos =SocialPersonList[ListBox_SocialPeople.SelectedIndex].iEconomicPosition;
                        RenderWindow.AddText("C\ne\nn\nt\nr\na\nl\nl\ny", 14+TrackBar_TextSize.Value, 50, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                        RenderWindow.AddText("P\nl\na\nn\nn\ne\nd", 14+TrackBar_TextSize.Value, 70, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                        RenderWindow.AddText("F\nr\ne\ne", 14+TrackBar_TextSize.Value, 670, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                        RenderWindow.AddText("E\nn\nt\ne\nr\np\nr\ni\ns\ne", 14+TrackBar_TextSize.Value, 690, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                    } else if (YAxisLabel == "S\no\nc\ni\na\nl") {
                        iXPos = SocialPersonList[ListBox_SocialPeople.SelectedIndex].iSocialPosition;
                        RenderWindow.AddText("C\no\nl\nl\ne\nc\nt\ni\nv\ni\ns\nt", 14+TrackBar_TextSize.Value, 70, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                        RenderWindow.AddText("I\nn\nd\ni\nv\ni\nd\nu\na\nl\ni\ns\nt", 14+TrackBar_TextSize.Value, 670, iTopMargin, 50, (iBottomMargin)-(iTopMargin), Color.Black);
                    }
                    //Console.WriteLine("Rendering at natural coordinates: ("+iXPos+", "+iYPos+").");
                    iXPos *= 50;
                    iYPos *= 50;
                    iXPos += 395;
                    iYPos += 395;
                    //Console.WriteLine("Rendering at computer coordinates: ("+iXPos+", "+iYPos+").");
                    string TempText = SocialPersonList[ListBox_SocialPeople.SelectedIndex].sName.Replace(' ', '\n');
                    RenderWindow.AddText(TempText, 16+TrackBar_TextSize.Value, 0, 0, 250, 120, Color.Black);
                    RenderWindow.AddEllipse(iXPos-10, iYPos-10, 20, 20, Color.Black);
                    RenderWindow.AddEllipse(iXPos-8, iYPos-8, 16, 16, Color.Red);
                } else {
                    MessageBox.Show("The selected entry contains only one spectrum and cannot be rendered in two dimmensions.", "Error");
                }

            }

        }
        private void TreeView_Notes_KeyUp(object sender, KeyEventArgs e) {
            if (e.Control && e.KeyCode.ToString() == "C") {
                if ((TreeView_Notes.SelectedNode != null) && (TreeView_Notes.SelectedNode.Text != "None.")) {
                    e.Handled = true;
                    this.KeyPreview = true;
                    Clipboard.SetText(TreeView_Notes.SelectedNode.Text.Substring(2, TreeView_Notes.SelectedNode.Text.Length-2));
                }
            }
        }
        private void MenuBar_File_SaveAs_Click(object sender, EventArgs e) {
            if (SaveFileDialog.ShowDialog() == DialogResult.OK) {
                try {
                    FileStream FileParser = new FileStream(SaveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                    BinarySerializer.Serialize(FileParser, SocialPersonList);
                    FileParser.Close();
                    //Console.WriteLine("Serialization Done!");
                    sActiveFile = SaveFileDialog.FileName;
                    OpenFileDialog.FileName = SaveFileDialog.FileName;
                    bSavingRequired = false;
                } catch (Exception exception) {
                    MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                }
            }
        }
        private void MenuBar_File_Save_Click(object sender, EventArgs e) {
            if (sActiveFile == "") {
                if (SaveFileDialog.ShowDialog() == DialogResult.OK) {
                    try {
                        FileStream FileParser = new FileStream(SaveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                        BinarySerializer.Serialize(FileParser, SocialPersonList);
                        FileParser.Close();
                        //Console.WriteLine("Serialization Done!");
                        sActiveFile = SaveFileDialog.FileName;
                        OpenFileDialog.FileName = SaveFileDialog.FileName;
                        bSavingRequired = false;
                    } catch (Exception exception) {
                        MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                    }
                }
            } else {
                try {
                    FileStream FileParser = new FileStream(sActiveFile, FileMode.Create, FileAccess.Write);
                    BinarySerializer.Serialize(FileParser, SocialPersonList);
                    FileParser.Close();
                    //Console.WriteLine("Serialization Done!");
                    bSavingRequired = false;
                } catch (Exception exception) {
                    MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                }
            }
        }
        private void MenuBar_File_Open_Click(object sender, EventArgs e) {
            if (OpenFileDialog.ShowDialog() == DialogResult.OK) { 
                try {
                    FileStream FileParser = new FileStream(OpenFileDialog.FileName, FileMode.Open, FileAccess.Read);
                    SocialPersonList = (List<SocialPerson>)BinarySerializer.Deserialize(FileParser);
                    FileParser.Close();
                    for (int i = 0; i < SocialPersonList.Count; i++) {
                        //Console.WriteLine(SocialPersonList[i].sName);
                        //Update Entries - All Previous Versions -> 1.01
                        if (SocialPersonList[i].sLatestActiveVersion == null) {
                            SocialPerson TempSocialPerson = SocialPersonList[i];
                            TempSocialPerson.sLatestActiveVersion = "1.01";
                            Console.WriteLine(TempSocialPerson.sName+" Updated to 1.01!");
                            if (TempSocialPerson.iPoliticalPosition != -100) TempSocialPerson.iPoliticalPosition = (Int16)(TempSocialPerson.iPoliticalPosition*-1);
                            SocialPersonList[i] = TempSocialPerson;
                        }
                        //Update Entries - 1.01, 1.02 -> 1.03
                        if (SocialPersonList[i].sLatestActiveVersion == "1.01") {
                            SocialPerson TempSocialPerson = SocialPersonList[i];
                            TempSocialPerson.sLatestActiveVersion = "1.03";
                            Console.WriteLine(TempSocialPerson.sName+" Updated to 1.03!");
                            SocialPersonList[i] = TempSocialPerson;
                        }
                    }
                    ListBox_SocialPeople.Items.Clear();
                    foreach (SocialPerson aPerson in SocialPersonList) {
                        if (aPerson.EntryType == TypeOfEntry.Ideology) {
                            ListBox_SocialPeople.Items.Add("[I] "+aPerson.sName);
                        } else if (aPerson.EntryType == TypeOfEntry.Philosopher) {
                            ListBox_SocialPeople.Items.Add("[P] "+aPerson.sName);
                        }
                    }
                    //Console.WriteLine("Deserialization Done!");
                    sActiveFile = OpenFileDialog.FileName;
                    SaveFileDialog.FileName = OpenFileDialog.FileName;
                    bSavingRequired = false;
                } catch (Exception exception) {
                    MessageBox.Show(exception.Message, "BinarySerializer Deserialization Error");
                }
            }
        }
        private void MenuBar_Misc_Updates_Click(object sender, EventArgs e) {
            string sUpdateString = WebParser.DownloadString(sUpdateServer);
            //sUpdateString = sUpdateString.Substring(0, sUpdateString.IndexOf(Environment.NewLine));
            //Console.WriteLine(sUpdateString);
            if (Double.Parse(sUpdateString) > Double.Parse(sSoftwareVersion)) {
                DialogResult UpdateDialogResult = MessageBox.Show("There is a new version available, would you like to update?\nAll unsaved changes will be lost.", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (UpdateDialogResult == DialogResult.Yes) {
                    // Prepare the process to run
                    ProcessStartInfo start = new ProcessStartInfo();
                    // Enter in the command line arguments, everything you would enter after the executable name itself
                    start.Arguments = '"'+Application.ExecutablePath+'"';
                    start.Verb = "runas";
                    //start.Arguments = '"'+Application.ExecutablePath+'"';
                    //Console.WriteLine(Application.ExecutablePath);
                    // Enter the executable to run, including the complete path
                    start.FileName = Application.StartupPath+"\\Updater.exe";
                    // Do you want to show a console window?
                    start.WindowStyle = ProcessWindowStyle.Normal;
                    start.CreateNoWindow = false;
                    // Run the external process & wait for it to finish
                    using (Process proc = Process.Start(start));

                    Application.Exit();
                }
            } else {
                MessageBox.Show("Your version is up to date!", "No update found.");
            } 
        }
        private void MenuBar_Misc_Credits_Click(object sender, EventArgs e) {
            MessageBox.Show("-----------------------------------\nSocial Studies Software Version "+sSoftwareVersion+"\n-----------------------------------\n\nProgramming & Development:\n     Jameson R. Grieve\n     JamesonRGrieve@GMail.com\n\nUses Microsoft .Net Framework", "Credits");
        }
        private void SocialStudiesSoftware_FormClosing(object sender, FormClosingEventArgs e) {
            if (bSavingRequired) {
                DialogResult ExitDialogResult = MessageBox.Show("Would you like to save your changes? Active edits will not be saved, please either create a new entry or overwrite an existing one if you have made changes.", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Stop);
                if (ExitDialogResult == DialogResult.Yes) {
                    SaveFileDialog.ShowDialog();
                    if (SaveFileDialog.FileName == "") {
                        MessageBox.Show("You have not designated a file!", "Error");
                        e.Cancel = true;
                    } else {
                        try {
                            FileStream FileParser = new FileStream(SaveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                            BinarySerializer.Serialize(FileParser, SocialPersonList);
                            FileParser.Close();
                            //Console.WriteLine("Serialization Done!");
                        } catch (Exception exception) {
                            MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                            e.Cancel = true;
                        }
                    }
                } else if (ExitDialogResult == DialogResult.Cancel) {
                    e.Cancel = true;
                }
            }
        }
        private void TreeView_Notes_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {
            if (e.Button == System.Windows.Forms.MouseButtons.Right) {
                TreeView_Notes.SelectedNode = e.Node;
                if (TreeView_Notes.SelectedNode.Level == 1) {
                    String SelectedNodeText = TreeView_Notes.SelectedNode.Text.Substring(0, TreeView_Notes.SelectedNode.Text.Length-1);
                    String ItemOneText = TreeView_Notes_CM_LvlOne.Items[0].Text;
                    String ItemTwoText = TreeView_Notes_CM_LvlOne.Items[1].Text;
                    String ItemThreeText = TreeView_Notes_CM_LvlOne.Items[2].Text;
                    TreeView_Notes_CM_LvlOne.Items[0].Text = ItemOneText+SelectedNodeText;
                    TreeView_Notes_CM_LvlOne.Items[1].Text = ItemTwoText+SelectedNodeText+"s";
                    TreeView_Notes_CM_LvlOne.Items[2].Text = ItemThreeText+SelectedNodeText+"s";
                    TreeView_Notes_CM_LvlOne.Show(Cursor.Position);
                } else if ((TreeView_Notes.SelectedNode.Level == 2) && (TreeView_Notes.SelectedNode.Text != "None.")) {
                    if (TreeView_Notes.SelectedNode.Parent.Text == "Biographical Notes") {
                        TreeView_Notes_CM_LvlTwo_Move_Category_Biographical.Visible = false;
                    } else if (TreeView_Notes.SelectedNode.Parent.Text == "Political Notes") {
                        TreeView_Notes_CM_LvlTwo_Move_Category_Political.Visible = false;
                    } else if (TreeView_Notes.SelectedNode.Parent.Text == "Economic Notes") {
                        TreeView_Notes_CM_LvlTwo_Move_Category_Economic.Visible = false;
                    } else if (TreeView_Notes.SelectedNode.Parent.Text == "Social Notes") {
                        TreeView_Notes_CM_LvlTwo_Move_Category_Social.Visible = false;
                    }
                    TreeView_Notes_CM_LvlTwo.Show(Cursor.Position);
                }
            }
        }
        private void TreeView_Notes_CM_LvlOne_Parse_Click(object sender, EventArgs e) {
            DialogResult ExitDialogResult = MessageBox.Show("The program will now show you a text entry form.\nUpon submitting it, the program will make its best attempt to interpret your notes and add them to the selected category.", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (ExitDialogResult == DialogResult.Yes) {
                string sUserText = TextForm.ShowDialog(true);
                if (sUserText != "Enter your notes here, one per line. Dashes, leading and trailing spaces will be ignored, other punctuation will be included.") {
                    string[] sUserNotes = sUserText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    foreach (string sUserNote in sUserNotes) {
                        if (sUserNote.Trim() != "") {
                            if (sUserNote.Substring(0, 1) == "-") {
                                sNewNote = sUserNote.Substring(1, sUserNote.Length-1);
                            } else {
                                sNewNote = sUserNote;
                            }
                            sNewNote = sNewNote.Trim();
                            sNewNote = "- " + sNewNote.Substring(0, 1).ToUpper() + sNewNote.Substring(1, sNewNote.Length - 1);
                            NewTreeNode = new TreeNode(sNewNote);
                            if (TreeView_Notes.SelectedNode.Nodes[0].Text == "None.") {
                                TreeView_Notes.SelectedNode.Nodes.Clear();
                            }
                            TreeView_Notes.SelectedNode.Nodes.Add(NewTreeNode);
                        }
                    }
                }
            }
        }
        private void TreeView_Notes_CM_LvlOne_Closed(object sender, ToolStripDropDownClosedEventArgs e) {
            TreeView_Notes_CM_LvlOne.Items[0].Text = "Create A New ";
            TreeView_Notes_CM_LvlOne.Items[1].Text = "Parse External Notes As ";
            TreeView_Notes_CM_LvlOne.Items[2].Text = "Clear All ";
        }
        private void TreeView_Notes_CM_LvlOne_Create_Click(object sender, EventArgs e) {
            string sUserText = TextForm.ShowDialog(false);
            if (sUserText.Trim() != "") {
                sNewNote = "- " + sUserText.Substring(0, 1).ToUpper() + sUserText.Substring(1, sUserText.Length - 1);
                NewTreeNode = new TreeNode(sNewNote);
                if (TreeView_Notes.SelectedNode.Nodes[0].Text == "None.") {
                    TreeView_Notes.SelectedNode.Nodes.Clear();
                }
                TreeView_Notes.SelectedNode.Nodes.Add(NewTreeNode);
            }
            NewTreeNode = new TreeNode(sUserText);
        }
        private void TreeView_Notes_CM_LvlOne_Clear_Click(object sender, EventArgs e) {
            TreeView_Notes.SelectedNode.Nodes.Clear();
            NewTreeNode = new TreeNode("None.");
            TreeView_Notes.SelectedNode.Nodes.Add(NewTreeNode);
        }
        private void TreeView_Notes_CM_LvlTwo_Edit_Click(object sender, EventArgs e) {
            TreeView_Notes.LabelEdit = true;
            TreeView_Notes.SelectedNode.BeginEdit();
        }
        private void TreeView_Notes_CM_LvlTwo_Delete_Click(object sender, EventArgs e) {
            if (TreeView_Notes.SelectedNode.Parent.Nodes.Count == 1) {
                TreeNode NewNode = new TreeNode("None.");
                TreeView_Notes.SelectedNode.Parent.Nodes.Add(NewNode);
            }
            TreeView_Notes.SelectedNode.Remove();
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Up_Click(object sender, EventArgs e) {
            Extensions.TreeView_MoveUp(TreeView_Notes.SelectedNode);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Top_Click(object sender, EventArgs e) {
            Extensions.TreeView_MoveTop(TreeView_Notes.SelectedNode);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Down_Click(object sender, EventArgs e) {
            Extensions.TreeView_MoveDown(TreeView_Notes.SelectedNode);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Bottom_Click(object sender, EventArgs e) {
            Extensions.TreeView_MoveBottom(TreeView_Notes.SelectedNode);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Category_Exec(object sender, Int32 index) {
            if (TreeView_Notes.SelectedNode.Parent.Nodes.Count == 1) {
                TreeNode NewNode = new TreeNode("None.");
                TreeView_Notes.SelectedNode.Parent.Nodes.Add(NewNode);
            }
            MovingTreeNode = TreeView_Notes.SelectedNode;
            TreeView_Notes.SelectedNode.Remove();
            if (TreeView_Notes.Nodes[0].Nodes[index].Nodes[0].Text == "None.") {
                TreeView_Notes.Nodes[0].Nodes[index].Nodes.Clear();
            }
            TreeView_Notes.Nodes[0].Nodes[index].Nodes.Add(MovingTreeNode);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Category_Biographical_Click(object sender, EventArgs e) {
            TreeView_Notes_CM_LvlTwo_Move_Category_Exec(sender, 0);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Category_Political_Click(object sender, EventArgs e) {
            TreeView_Notes_CM_LvlTwo_Move_Category_Exec(sender, 1);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Category_Economic_Click(object sender, EventArgs e) {
            TreeView_Notes_CM_LvlTwo_Move_Category_Exec(sender, 2);
        }
        private void TreeView_Notes_CM_LvlTwo_Move_Category_Social_Click(object sender, EventArgs e) {
            TreeView_Notes_CM_LvlTwo_Move_Category_Exec(sender, 3);
        }
        private void TreeView_Notes_CM_LvlTwo_Closed(object sender, ToolStripDropDownClosedEventArgs e) {
            TreeView_Notes_CM_LvlTwo_Move_Category_Biographical.Visible = true;
            TreeView_Notes_CM_LvlTwo_Move_Category_Political.Visible = true;
            TreeView_Notes_CM_LvlTwo_Move_Category_Economic.Visible = true;
            TreeView_Notes_CM_LvlTwo_Move_Category_Social.Visible = true;
        }
        private void TreeView_Notes_AfterLabelEdit(object sender, NodeLabelEditEventArgs e) {
            TreeView_Notes.LabelEdit = false;

        }
        //-END FORM CLASS-

        public static class TextForm {
            public static string ShowDialog(Boolean bBig) {
                if (bBig) {
                    Form TextPrompt = new Form_Text();
                    TextPrompt.SetBounds(0, 0, 400, 250);
                    TextPrompt.MinimumSize = new System.Drawing.Size(400, 250);
                    TextPrompt.MaximumSize = new System.Drawing.Size(400, 250);
                    TextPrompt.Text = "Note Parser";
                    TextPrompt.Controls.Find("TextBox_Text", true)[0].Text = "Enter your notes here, one per line. Dashes, leading and trailing spaces will be ignored, other punctuation will be included.";
                    TextPrompt.ShowDialog();
                    return TextPrompt.Controls.Find("TextBox_Text", true)[0].Text;
                } else {
                    Form TextPrompt = new Form_Text();
                    TextPrompt.Text = "New Note";
                    TextPrompt.MinimumSize = new System.Drawing.Size(500, 150);
                    TextPrompt.MaximumSize = new System.Drawing.Size(500, 150);
                    TextPrompt.ShowDialog();
                    return TextPrompt.Controls.Find("TextBox_Text", true)[0].Text;
                }
            }
        }
        public static class Select2DSpectrumsForm {
            public static List<int> ShowDialog(SocialPerson ThePerson) {
                Form Select2DSpectrumsPrompt = new Form_Select2DSpectrums();
                List<Int32> EnabledList = new List<Int32>();
                EnabledList.Add(1);
                EnabledList.Add(2);
                EnabledList.Add(3);
                if (ThePerson.iPoliticalPosition == -100) {
                    EnabledList.Remove(1);
                }
                if (ThePerson.iEconomicPosition == -100) {
                    EnabledList.Remove(2);
                }
                if (ThePerson.iSocialPosition == -100) {
                    EnabledList.Remove(3);
                } 
                if (EnabledList.Count == 3) {
                    EnabledList.Clear();
                    Select2DSpectrumsPrompt.ShowDialog();
                    if (Select2DSpectrumsPrompt.Controls.Find("HiddenString", true)[0].Text == "1,2") {
                        EnabledList.Clear();
                        EnabledList.Add(1);
                        EnabledList.Add(2);
                        return EnabledList;
                    } else if (Select2DSpectrumsPrompt.Controls.Find("HiddenString", true)[0].Text == "2,3") {
                        EnabledList.Clear();
                        EnabledList.Add(2);
                        EnabledList.Add(3);
                        return EnabledList;
                    } else if (Select2DSpectrumsPrompt.Controls.Find("HiddenString", true)[0].Text == "1,3") {
                        EnabledList.Clear();
                        EnabledList.Add(1);
                        EnabledList.Add(3);
                        return EnabledList;
                    } else if (Select2DSpectrumsPrompt.Controls.Find("HiddenString", true)[0].Text == "nil") {
                        return new List<Int32>();
                    } else {
                        MessageBox.Show("This label is only supposed to contain one of three values. What did you do...?", "Error");
                        return new List<Int32>();
                    }
                } else if (EnabledList.Count == 2) {
                    return EnabledList;
                } else {
                    return new List<Int32>();
                }
            }
        }
        public static class StudyForm {
            public static void ShowDialog() {
                Form StudyPrompt = new Form_Study(SocialPersonList);
                StudyPrompt.Show();
            }
        }

        private void RadioButton_Philosopher_Click(object sender, EventArgs e) {
            RadioButton_Philosopher.Checked = true;
            RadioButton_Ideology.Checked = false;
        }

        private void RadioButton_Ideology_Click(object sender, EventArgs e) {
            RadioButton_Ideology.Checked = true;
            RadioButton_Philosopher.Checked = false;
        }

        private void testToolStripMenuItem1_Click(object sender, EventArgs e) {
            if (SocialPersonList.Count > 0) {
                StudyForm.ShowDialog();
            } else {
                MessageBox.Show("You have no entries to study!", "Error!");
            }
        }

        private void ToolStrip_Study_AllowClose_Click(object sender, EventArgs e) {
            ToolStripMenuItem StripSender = (ToolStripMenuItem)sender;
            bStudyAllowClose = StripSender.Checked;
            Console.WriteLine(bStudyAllowClose.ToString());
        }
    }
    public static class Extensions {
    //http://stackoverflow.com/questions/2203975/move-node-in-tree-up-or-down
        public static void TreeView_MoveUp(this TreeNode node) {
            TreeNode parent = node.Parent;
            TreeView view = node.TreeView;
            if (parent != null) {
                int index = parent.Nodes.IndexOf(node);
                if (index > 0) {
                    parent.Nodes.RemoveAt(index);
                    parent.Nodes.Insert(index - 1, node);
                }
            } else if (node.TreeView.Nodes.Contains(node)) { //root node
                int index = view.Nodes.IndexOf(node);
                if (index > 0) {
                    view.Nodes.RemoveAt(index);
                    view.Nodes.Insert(index - 1, node);
                }
            }
        }
        public static void TreeView_MoveTop(this TreeNode node) {
            TreeNode parent = node.Parent;
            TreeView view = node.TreeView;
            if (parent != null) {
                int index = parent.Nodes.IndexOf(node);
                if (index > 0) {
                    parent.Nodes.RemoveAt(index);
                    parent.Nodes.Insert(0, node);
                }
            } else if (node.TreeView.Nodes.Contains(node)) {//root node
                int index = view.Nodes.IndexOf(node);
                if (index > 0) {
                    view.Nodes.RemoveAt(index);
                    view.Nodes.Insert(0, node);
                }
            }
        }
        public static void TreeView_MoveDown(this TreeNode node) {
            TreeNode parent = node.Parent;
            TreeView view = node.TreeView;
            if (parent != null) {
                int index = parent.Nodes.IndexOf(node);
                if (index < parent.Nodes.Count -1) {
                    parent.Nodes.RemoveAt(index);
                    parent.Nodes.Insert(index + 1, node);
                }
            } else if (view != null && view.Nodes.Contains(node)) {//root node
                int index = view.Nodes.IndexOf(node);
                if (index < view.Nodes.Count - 1) {
                    view.Nodes.RemoveAt(index);
                    view.Nodes.Insert(index + 1, node);
                }
            }
        }
        public static void TreeView_MoveBottom(this TreeNode node) {
            TreeNode parent = node.Parent;
            TreeView view = node.TreeView;
            if (parent != null) {
                int index = parent.Nodes.IndexOf(node);
                if (index < parent.Nodes.Count -1) {
                    parent.Nodes.RemoveAt(index);
                    parent.Nodes.Insert(parent.Nodes.Count, node);
                }
            } else if (view != null && view.Nodes.Contains(node)) { //root node
                int index = view.Nodes.IndexOf(node);
                if (index < view.Nodes.Count - 1) {
                    view.Nodes.RemoveAt(index);
                    view.Nodes.Insert(parent.Nodes.Count, node);
                }
            }
        }
    }
}
//-END FORM NAMESPACE-

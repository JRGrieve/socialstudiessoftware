﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocialStudiesSoftware {
    public partial class Form_Text : Form {
        public Form_Text() {
            InitializeComponent();
        }

        private void Button_Submit_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TextForm_Load(object sender, EventArgs e) {
            if (TextBox_Text.Text != "") {
                Label_Label.Text = "Note Parser";
                TextBox_Text.Multiline = true;
                TextBox_Text.ScrollBars = ScrollBars.Both;
            } else {
                Label_Label.Text = "New Note";
                
            }
        }


    }
}
